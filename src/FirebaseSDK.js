import firebase from 'firebase'


class FirebaseSDK {
    constructor(){
        if(!firebase.apps.length)
        {

        firebase.initializeApp({
            apiKey: "AIzaSyDTPf6CZsG2LmoD9PsknjApu3TGf1SBupU",
            authDomain: "myreact-d03d4.firebaseapp.com",
            databaseURL: "https://myreact-d03d4.firebaseio.com",
            projectId: "myreact-d03d4",
            storageBucket: "myreact-d03d4.appspot.com",
            messagingSenderId: "386883955873",
            appId: "1:386883955873:web:03e26a650204ba6b4a94e0",
            measurementId: "G-GYGFWBCCT3"})
        };
    }

    login = async (user, success_callback,failed_callback)=>{
        await firebase
        .auth()
        .signInWithEmailAndPassword(user.email,user.password)
        .then(success_callback,failed_callback)
    }
    
}

const firebaseSDK = new FirebaseSDK();
export default firebaseSDK;