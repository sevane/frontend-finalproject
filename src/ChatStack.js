import React from 'react'
import {Platform, KeyboardAvoidingView, View, Text,SafeAreaView} from "react-native"
import { GiftedChat } from 'react-native-gifted-chat';
import {Fire} from "./Fire"
import firebase from 'firebase'


export class ChatStack extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    title: (navigation.state.params || {}).name || 'Chat!',
  });



  
  state = {
    messages: [
      {
        _id: 1,
        text: 'Hello developer',
        createdAt: new Date(Date.UTC(2016, 7, 30, 17, 20, 0)),
        user: {
          _id: (firebase.auth().currentUser|| {}).uid,
          name: 'React Native',
          avatar: 'https://facebook.github.io/react/img/logo_og.png',
        },
      },
    ]
  };

  get user() {
    return {
     name: "sev",
      _id: (firebase.auth().currentUser || {}).uid,
      
    };
  }

  render() {
       const chat=<GiftedChat messages={this.state.messages} onSend={Fire.shared.send} user={firebase.auth().currentUser}/>;

        if(Platform.OS=='android'){
            return(
                <KeyboardAvoidingView style={{flex:1}}behavior="padding" keyboardVerticalOffset={0} enabled>
                    {chat}
                </KeyboardAvoidingView>
            );
        }
    return<SafeAreaView style={{flex:1}}>{chat}</SafeAreaView>; 
  }

  componentDidMount() {
    Fire.shared.on(message =>
      this.setState(previousState => ({
        messages: GiftedChat.append(previousState.messages, message),
      }))
    );
  }
  componentWillUnmount() {
    Fire.shared.off();
  }
}

