import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity, FlatList,Linking} from 'react-native';


export class Sos extends React.Component{

RequestHelp=()=>{
    alert("Your Request is successfully sent! ")
}
    render(){
        return(
            <View style={{flex:1,}}>

            <View style={styles.header}>
                    <Text style={styles.headerTitle}>Request Help</Text>
            </View>
            <View style={{flex:1,alignItems:'center', justifyContent:'center'}}>
                <Image source={require ('./assets/thumbnail.jpeg')} style={{flex:1,width:380, height:350,}} 
                />
                <TouchableOpacity style={styles.buttonContainer}  onPress={() => alert("Your Request is successfully sent! ")}>
                <Text>Help</Text> 
              </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        flex: 1, 
       
    },
    header: {
        paddingTop: 14,
        paddingBottom: 16,
        backgroundColor: "#FFF",
        alignItems: "center",
        justifyContent: "center",
        borderBottomWidth: 1,
        borderBottomColor: "#EBECF4",
        shadowColor: "#454D65",
        shadowOffset: { height: 5 },
        shadowRadius: 15,
        shadowOpacity: 0.2,
        zIndex: 10
    },
    headerTitle: {
        fontSize: 20,
        color:'#45f74b',
        fontWeight: "700",
       
    },
    buttonContainer: {
        marginBottom:10,
        marginTop:10,
        height:45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
       width:250,
        borderRadius:30,
        backgroundColor: "#45f74b",
      },
})