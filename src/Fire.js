
import firebase from 'firebase'; // 4.8.1

export class Fire {
  constructor() {
    this.init();
    this.observeAuth();
  }

  
    init =()=>
    {
        if(!firebase.apps.length)
        {var firebaseConfig = {
            apiKey: "AIzaSyDTPf6CZsG2LmoD9PsknjApu3TGf1SBupU",
            authDomain: "myreact-d03d4.firebaseapp.com",
            databaseURL: "https://myreact-d03d4.firebaseio.com",
            projectId: "myreact-d03d4",
            storageBucket: "myreact-d03d4.appspot.com",
            messagingSenderId: "386883955873",
            appId: "1:386883955873:web:03e26a650204ba6b4a94e0",
            measurementId: "G-GYGFWBCCT3"
          };
          // Initialize Firebase
          firebase.initializeApp(firebaseConfig);
    }
  };

  
observeAuth = () =>
    firebase.auth().onAuthStateChanged(this.onAuthStateChanged);

  onAuthStateChanged = user => {
    if (user) {
      try {
       console.log("chatUSer----->",user)
      } catch ({ message }) {
        alert("error");
      }
    }
  };

  get uid() { console.log("getuser----->",(firebase.auth().currentUser || {}).uid)
    return (firebase.auth().currentUser || {}).uid;
  }

  get ref() {
    return firebase.database().ref('messages');
  }

  parse = snapshot => {
    const { timestamp: numberStamp, text, user } = snapshot.val();
    const { key: _id } = snapshot;
    const timestamp = new Date(numberStamp);
    const message = {
      _id,
      timestamp,
      text,
      user,
    };
    return message;
  };

  on = callback =>
    this.ref
      .limitToLast(20)
      .on('child_added', snapshot => callback(this.parse(snapshot)));

  get timestamp() {
    return firebase.database.ServerValue.TIMESTAMP;
  }
  // send the message to the Backend
  send = messages => {
    for (let i = 0; i < messages.length; i++) {
      const { text, user } = messages[i];
      const message = {
        text,
        user,
        timestamp: this.timestamp,
      };
      this.append(message);
    }
  };

  append = message => this.ref.push(message);

  // close the connection to the Backend
  off() {
    this.ref.off();
  }
}

Fire.shared = new Fire();
export default Fire;