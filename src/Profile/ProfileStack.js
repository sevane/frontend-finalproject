
import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity, FlatList,Linking} from 'react-native';
import axios from 'axios'
import Icon from 'react-native-vector-icons/Ionicons'
import AsyncStorage from '@react-native-community/async-storage';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import {MyPostsStack} from './MyPostsScreen'
import {EditProfile} from './EditProfileScreen'
import { useNavigation } from '@react-navigation/native';
import {AuthContext} from '/home/sefactory-02/MyNewReact/frontend-finalproject/src/utils.js'

const Drawer = createDrawerNavigator();
//const { signOut } = React.useContext(AuthContext);
function CustomDrawerContent(props) {const { signOut } = React.useContext(AuthContext);
  return (
    <DrawerContentScrollView {...props}>
      <DrawerItemList {...props} />
      <DrawerItem
        label="Log out"
        onPress={signOut}
      />
      {/* <DrawerItem
        label="Toggle drawer"
        onPress={() => props.navigation.toggleDrawer()}
      /> */}
    </DrawerContentScrollView>
  );
}
export function ProfileStack() {
  return (
    <Drawer.Navigator drawerContent={props => <CustomDrawerContent {...props} />} 
   
    drawerPosition='right'
    drawerContentOptions={{
      activeTintColor:'#45f74b'
    }}>
      <Drawer.Screen name="Profile" component={MyProfile} />
      <Drawer.Screen name="Edit Profile" component={EditProfile} />
      <Drawer.Screen name="Notifications" component={Notifications} />
      <Drawer.Screen name="My Posts" component={MyPostsStack} />
    </Drawer.Navigator>
  );
}
function Notifications() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Notifications Screen</Text>
    </View>
  );
}






//create component
export default function(props) {
  const navigation = useNavigation();
  return <MyBackButton {...props} navigation={navigation} />;
}
export  class MyProfile extends Component {
  constructor()
 {
     super()
     this.state={
         isLoading: true,
         data:'',
         
     }
     console.disableYellowBox=true
 }

 
componentDidMount(){
  let UID
 console.log("inside profile getdata")
  
    console.log("profile get Data inside try")
    
      AsyncStorage.getItem('userID').then((response)=>{
      UID = response;
      this.makeRequest(UID)
      console.log("user ID inside profile:",UID)

})
    
 
}
getData = async () => {
 
  let UID
 console.log("inside profile getdata")
  try {
    console.log("profile get Data inside try")
    
     await AsyncStorage.getItem('userID').then((response)=>{
     return UID = response;
     
})
    console.log("user ID inside profile:",UID)
   
  } catch(e) {
    // error reading value
    console.log("get data error",e)
  }

 
}


makeRequest = (uid)=>{
  axios.get('http://10.0.2.2:8000/api/user/'+uid)
  .then(res=>{
    console.log('code profile response--->',res)
    this.setState({
      data: [res.data]
    })
})
}

Dial=(phone)=>{
 Linking.openURL(`tel:${phone}`)
 //Linking.openURL('tel:09876543')
}
  render() {
    const { navigation } = this.props;
    return (<View>
    <View style={styles.head}>
      <Text style={styles.headerTitle}>Profile</Text>
      <TouchableOpacity style={styles.IconBtn}  onPress={() => navigation.openDrawer()}  >
      <Icon name="ios-menu" size={24}  backgroundColor="blue"  color="#73788B" left={100} ></Icon>
      </TouchableOpacity>
    </View>
      <FlatList data={this.state.data} 
                            keyExtractor={(item)=>JSON.stringify(item.id)}
                            onEndReached={this.makeRequest}
                            onEndReachedThreshold={1}
                            renderItem={
                              ({item})=>
      <View style={styles.container}>
           
          <View style={styles.header}>
            
          <Image source ={{uri: item.avatar}} style={styles.avatar}/></View>
          <View style={styles.body}>
            <View style={styles.bodyContent}>
            <Text style ={styles.name}>{item.name}</Text>
             
              <Text style={styles.description}>{item.bio}</Text>
              <View style={styles.Card}>
               <Text style={styles.CardText}>Tel: </Text>
              <Text style={styles.CardText} onPress={()=>this.Dial(item.phone)} >{item.phone}</Text>
              </View>
              <View style={styles.Card}>
              <Text style={styles.CardText}>From: </Text>
                <Text style={styles.CardText}>{item.address}</Text>
              </View>
              

            </View>
        </View>
      
      </View>}/></View>
    );
  }
}

const styles = StyleSheet.create({
  header:{
    flex:2,
    backgroundColor: "white",
    height:200,
  },
  avatar: {
    width: 180,
    height: 180,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "#45f74b",
    marginLeft:120,
    marginBottom:110,
    position:'absolute',
    marginTop:10,
  },
  
  body:{
    marginTop:null,
  },
  bodyContent: {
    flex: 1,
    alignItems: 'center',
    padding:10,
  },
  name:{
    fontSize:28,
    color: "#696969",
    fontWeight: "600"
  },
  info:{
    fontSize:16,
    color: "#696969",
    marginTop:10
  },
  description:{
    fontSize:24,
    color: "#696969",
    marginTop:10,
    textAlign: 'center',
    marginBottom:20
  },
  buttonContainer: {
    marginTop:10,
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    
    width:250,
    borderRadius:30,
    backgroundColor: "#45f74b",
  },
  Card:{justifyContent:'center',
  
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius:30,
    borderBottomWidth: 1,
    width:160,
    height:45,
    marginBottom:10,
    flexDirection: 'row',
    alignItems:'center',

  },
  CardText:{
    fontSize:20,
    //left:20,
    color:"green"
  },
  button: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 10
  },
  head: {
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: "#FFF",
    alignItems: "center",
    justifyContent: "center",
    borderBottomWidth: 10,
    borderBottomColor: "#EBECF4",
    shadowColor: "#454D65",
    shadowOffset: { height: 5 },
    shadowRadius: 15,
    shadowOpacity: 0.2,
    zIndex: 10,
   

    
},
headerTitle: {
    fontSize: 20,
    color:'#45f74b',
    fontWeight: "700",
left:20
},
 
  IconBtn: {
    left:140,
    alignItems: "center",
    backgroundColor: "transparent",
    padding: 10
  },


});
 