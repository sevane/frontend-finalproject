
import React from 'react';
import {View, Text,StyleSheet, FlatList,Image,TouchableOpacity, Dimensions, ActivityIndicator, Button} from "react-native"
import axios from 'axios'
import Icon from 'react-native-vector-icons/Ionicons'
import { createStackNavigator } from "@react-navigation/stack";
import { useNavigation } from '@react-navigation/native';
//import {ChatStack} from "../ChatStack"
import AsyncStorage from '@react-native-community/async-storage';
import {EditPost} from './EditPostScreen'
 const Stack = createStackNavigator();
 
 export const MyPostsStack =()=>{
    
      return(
          <Stack.Navigator>
              <Stack.Screen name= "My Posts" options={{header: ()=>null}} component={MyPosts}></Stack.Screen>
              <Stack.Screen name= "Edit Post"  component={EditPost}></Stack.Screen>
              
          </Stack.Navigator>
      );
 }
 function editpost (){
   //  data= this.props.route.params
    return(<View style ={{
        flex: 1, alignItems: "center",
        justifyContent: "center"
                        }}>
        <Text>edit </Text>
         </View>);
}
function Chat (){
    return(<View style ={{
        flex: 1, alignItems: "center",
        justifyContent: "center"
                        }}>
        <Text>this is Hello</Text>
         </View>);
}
 
//create component
export default function(props) {
    const navigation = useNavigation();
    return <MyBackButton {...props} navigation={navigation} />;
  }
export  class MyPosts extends React.Component {

     
 constructor()
 {
     super()
     this.state={
         
         data:'',
       
     }
     console.disableYellowBox=true
 }
componentDidMount(){
    let UID
 console.log("inside profile getdata")
  
    console.log("profile get Data inside try")
    
      AsyncStorage.getItem('userID').then((response)=>{
      UID = response;
      this.makeRequest(UID)
      console.log("user ID inside profile:",UID)})
}
 makeRequest = (uid)=>{
     axios({method:'GET',
    url:'http://10.0.2.2:8000/api/UserPosts/'+uid})
     .then(res=>{
         this.setState({
             isLoading:false,
             data: [...this.state.data,...res.data]
         })
     }).catch(function (error) {
        this.setState({
            isLoading:false
        })
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        }
      })
 }

 
 

 render(){
  
   const { navigation } = this.props;
    return(
        <View style={styles.container}>
            <View style={styles.header}>
                    <Text style={styles.headerTitle}>My Posts</Text>
                    
            </View>
           <FlatList data={this.state.data} 
                            keyExtractor={(item)=>item.id}
                            
                            
                            onEndReachedThreshold={10}
                            renderItem={({item})=>
                                            <View style={styles.feedItem}>
                                           
                                            <View style={{flex:1}}>
                                                <View style={{ flexDirection: 'row', justifyContent:"space-between", alignItems:"center"}}>
                                                <TouchableOpacity style={styles.IconBtn}  onPress={()=>{this.props.navigation.navigate("Edit Post", {pID: item.id, title: item.title,content: item.content,picture: item.picture});}}  >
                                                <Icon name="ios-construct" size={24}  backgroundColor="blue"  color="#73788B" left={100} ></Icon>
                                                </TouchableOpacity>
                                                    {/* <Icon.Button name="ios-construct" size={24}  backgroundColor="transparent"  color="#73788B"  ></Icon.Button> */}
                                                    <Icon.Button name="ios-trash" size={24} backgroundColor="transparent"  color="#73788B" ></Icon.Button>
                                                </View> 
                                            
                                                <Text style={styles.post}>{item.title}</Text>
                                                <Text style={styles.post}>{item.content}</Text>
                                                <Image source={{uri: item.picture}} style={styles.postImage} resizeMode="cover"></Image>
                                                <View style={{flexDirection:'row'}}>
                                                    <Icon.Button name="ios-heart" size={24}  backgroundColor="transparent"  color="#73788B"  ></Icon.Button>
                                                    <Icon.Button name="ios-create" size={24} backgroundColor="transparent"  color="#73788B" onPress={()=>{navigation.navigate("Comments");}} ></Icon.Button>
                                                    <Icon.Button name="ios-chatbubbles" size={24} backgroundColor="transparent"  color="#73788B" onPress={()=>{this.props.navigation.navigate("ChatStack",{name: item.name});}} ></Icon.Button>
                                                </View>
                                            </View>
                                        </View>
                            }/>
        </View>
    )
}




}


const styles=StyleSheet.create({
    container:{
        flex: 1, 
       
    },
    header: {
        paddingTop: 14,
        paddingBottom: 16,
        backgroundColor: "#FFF",
        alignItems: "center",
        justifyContent: "center",
        borderBottomWidth: 1,
        borderBottomColor: "#EBECF4",
        shadowColor: "#454D65",
        shadowOffset: { height: 5 },
        shadowRadius: 15,
        shadowOpacity: 0.2,
        zIndex: 10
    },
    headerTitle: {
        fontSize: 20,
        color:'#45f74b',
        fontWeight: "700",
       
    },
    
    name:{
        fontSize:15,
        color:'green',
        marginTop:4
    },
    Load:{
        flex:1,
        padding:20,
        alignItems:'center',
        justifyContent:'center'
    },
    avatar:{
        width:36,
        height:36,
        borderRadius:18,
        marginRight:16

    },
    post:{
        marginTop:16,
        fontSize:14,
        color: "#838899"

    },
    postImage:{
        flex:1,
        width: null,
        height:150,
        borderRadius:5,
        marginVertical:16
    },
    feedItem:{
        backgroundColor:"#FFF",
        borderRadius:5,
        padding:8,
        flexDirection: "row",
        marginVertical:8
    },
    IconBtn: {
        left:140,
        alignItems: "center",
        backgroundColor: "transparent",
        
      },
    

})
