import * as React from 'react';
import{View,Text,TouchableHighlight,TextInput, TouchableOpacity,ScrollView, StatusBar, StyleSheet, FlatList, Dimensions, Image, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-crop-picker';
import {ActionSheet, Root} from 'native-base'
import axios from 'axios'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import { useNavigation } from '@react-navigation/native';

export default function(props) {
    const navigation = useNavigation();
    return <MyBackButton {...props} navigation={navigation} />;
  }
  
  export class EditProfile extends React.Component{
  
    constructor(){
      super()
      this.state={
        name:"",
        phone:"",
        address:'',
        bio:'',
        avatar:'',
        userid:''
      }
      this.handleChangeName = this.handleChangeName.bind(this)
      this.handleChangePhone = this.handleChangePhone.bind(this)
      this.handleChangeAddress = this.handleChangeAddress.bind(this)
      this.handleChangeBio = this.handleChangeBio.bind(this)
    }
  //input change handlers
  handleChangeName(inputText){this.setState({name: inputText})}
  handleChangePhone(inputText){this.setState({phone: inputText})}
  handleChangeAddress(inputText){this.setState({address: inputText})}
  handleChangeBio(inputText){this.setState({bio: inputText})}
  
    handleChangeContent(inputText){
      this.setState({
        content: inputText
      })
    }
  
  //ActionSheet handler 
    onClickAddImage=()=>{
      const btn = ['Take Photo','Choose Photo Library','Cancel'];
      ActionSheet.show(
        {options: btn, cancelButtonIndex:2, title:'Select a photo'},
       buttonIndex=>{
         switch(buttonIndex){
           case 0:
             this.takePhotoFromCamera();
             break;
             case 1:
              this.choosePhotoFromLibrary();
               break;
               default:
                 break;
         }}
      )}
  
  //image picker function to chose photo from library
    choosePhotoFromLibrary(){
      ImagePicker.openPicker({
        Width: 300,
        Height: 400,
        cropping: true,
      
      }).then(image => {
        console.log("image--->",image);
        this.setState({avatar: image.path})
      });
    }
  
    //image picker function to take a photo
    takePhotoFromCamera(){
      ImagePicker.openCamera({
        width: 300,
        height: 400,
        cropping: true,
      }).then(image => {
        console.log("image--->",image);
        this.setState({avatar: image.path})
      });
    }
  
  //fetch the user id from async storage and store it
    componentDidMount(){
      let UID
    AsyncStorage.getItem('userID').then((response)=>{
          UID = response;
          this.setState({
            userid: UID
          })
          console.log("user ID inside Edit:",this.state.userid)
          this.makeRequest(this.state.userid )
    })
    
  }

    makeRequest = (userID)=>{
    
      axios.get('http://10.0.2.2:8000/api/editUser/'+userID)
      .then(res=>{
        console.log('code profile response--->',res)
        this.setState({name: res.data.name,
          phone: res.data.phone,
          address: res.data.address,
          avatar: res.data.avatar,
          bio: res.data.bio})
    })
    console.log(this.state.avatar)
    }
  
    //add post (title,content, userid, picture to backend)
    onClickEditProfile=()=>{
      console.log("changed name-->",this.state.name)
      console.log("chaned phone-->",this.state.phone)
      console.log("changed img-->",this.state.avatar)
      console.log("changed id-->",this.state.userid)
      //pass params as form data
        var data = new FormData();
       data.append('name',this.state.name)
         data.append('phone',this.state.phone)
        data.append('avatar',this.state.avatar );
        data.append('address',this.state.address ); 
        data.append('bio',this.state.bio)
      
       axios.post("http://10.0.2.2:8000/api/updateUser/"+this.state.userid,data)
      .then(response=>{console.log("response-->",response)
      this.props.navigation.navigate("Profile")})
      .catch(function(error) {console.log("error--->",error)});
    }
  
  
    render(){
      return(
        <KeyboardAwareScrollView style={{flex:1}} >
         <View style={styles.container}>
          <StatusBar barStyle="dark-content"/>
          <ScrollView contentContainerStyle={{flex: 1, persistentScrollbar: true}}>
            <Root>
              <TouchableOpacity onPress={()=> this.onClickAddImage()}
              style={styles.avatar}>
                {this.state.avatar != '' && ( 
                  <Image source={{uri: this.state.avatar}}
                  style={{height:175, width:192, borderRadius:61}}/>
                  )} 
  
                  </TouchableOpacity>
                  </Root>
                  </ScrollView> 
                 
                  <View style={styles.inputContainer}>
<TextInput style={styles.inputs}
  placeholder="Name"
  underlineColorAndroid='transparent'
  value={this.state.name}
  onChangeText={this.handleChangeName}
/>
</View>
<View style={styles.inputContainer}>
<TextInput style={styles.inputs}
 placeholder="Phone"
 keyboardType="phone-pad"
 underlineColorAndroid='transparent'
 value={this.state.phone}
 onChangeText={this.handleChangePhone}
/>
</View>
<View style={styles.inputContainer}>
<TextInput style={styles.inputs}
 placeholder="Address"
 underlineColorAndroid='transparent'
 value={this.state.address}
 onChangeText={this.handleChangeAddress}
/>
</View>
                  <View style={styles.inputContent}>
                  <TextInput style={styles.inputs}
                    placeholder="Content"
                    multiline={true}
                    numberOfLines={4}
                    underlineColorAndroid='transparent'
                    defaultValue={this.state.bio}
                    onChangeText={this.handleChangeBio}
                  />
                  </View>
                  <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={this.onClickEditProfile} >
                  <Text style={styles.buttonText}>Save Changes</Text>
                  </TouchableHighlight>
       </View></KeyboardAwareScrollView>
      );
    }
  }
  
  const styles = StyleSheet.create({
    container: {
         flex: 1,
         justifyContent: 'center',
         alignItems: 'center',
         backgroundColor: '#DCDCDC',
       },
   button: {
     paddingHorizontal: 20,
     paddingVertical: 10,
     marginVertical: 10,
     borderRadius: 5
   },
   inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius:30,
    borderBottomWidth: 1,
    width:250,
    height:45,
    marginTop:10,
    marginBottom:10,
    flexDirection: 'row',
    alignItems:'center'
},
inputs:{
    height:45,
    marginLeft:16,
    borderBottomColor: '#FFFFFF',
    flex:1,
},
   inputContent: {
     borderBottomColor: '#F5FCFF',
     backgroundColor: '#FFFFFF',
     borderRadius:30,
     borderBottomWidth: 1,
     width:250,
     height:150,
     marginBottom:20,
     flexDirection: 'row',
     alignItems:'center'
   },
   inputs:{
       height:45,
       marginLeft:16,
       borderBottomColor: '#FFFFFF',
       flex:1,
      
   },
   content:{
    height:45,
    marginLeft:16,
    borderBottomColor: '#FFFFFF',
    flex:1,
  },
   content:{
     height:240,
     marginLeft:130,
     borderBottomColor: '#FFFFFF',
     flex:1,
   },
   inputIcon:{
     width:30,
     height:30,
     marginLeft:15,
     justifyContent: 'center'
   },
   buttonContainer: {
     height:45,
     flexDirection: 'row',
     justifyContent: 'center',
     alignItems: 'center',
     marginBottom:20,
     width:250,
     borderRadius:30,
   },
   loginButton: {
     backgroundColor: "#45f74b",
   },
   buttonText: {
     color: 'white',
   },
   avatar: {
    width: 200,
    height: 180,
     borderRadius: 63,
     borderWidth: 4,
     borderColor: "#45f74b",
     justifyContent: "center",
     backgroundColor:"white",
     marginTop:10,
     marginBottom:20
     
   },
   });