
import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity, FlatList,Linking} from 'react-native';
import axios from 'axios'
import Ionicons from 'react-native-vector-icons/Ionicons'
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome'

export  class AnimalDetail extends React.Component {
  constructor(props)
 {
     super(props)
     this.state={
         
         data:'',
         aID:'',
         uname: props.route.params.uname,
         avatar: props.route.params.avatar,
     }
     console.disableYellowBox=true
 }
componentDidMount(){
  const {aID}= this.props.route.params

    console.log('aID----->',aID)
    this.makeRequest(aID)
}



makeRequest = (aID)=>{
    
  axios.get('http://10.0.2.2:8000/api/showPet/'+aID)
  .then(res=>{
    console.log('code profile response--->',res)
    this.setState({
      data: [res.data]
    })
})
console.log(this.state.avatar)
}


  render() {

    return (
      <FlatList data={this.state.data} 
                            keyExtractor={(item)=>item.id}
                            onEndReached={this.makeRequest}
                            onEndReachedThreshold={1}
                            renderItem={
                              ({item})=>
      <View >
          
          <View style={styles.header}>
            
          <Image source ={{uri: item.animal_pic}} style={styles.avatar}/></View>
         

            <View style={styles.bodyContent}>
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <Text style={styles.description}>{item.description}</Text>
              </View>

            <View style={styles.detailContainer}>
              <Text style={styles.Txt} numberOfLines={1} ellipsizeMode="tail">Type:</Text>
              <Text style={styles.detail}>{item.type}</Text>
            </View>

            <View style={styles.detailContainer}>
            <Text style={styles.Txt} numberOfLines={1} ellipsizeMode="tail">Breed:</Text>
              {item.breed  != 'undefined'?
              <Text style={styles.detail}>{item.breed}</Text>:<Text style={styles.detail}>N/A</Text>}
            </View>

            <View style={styles.detailContainer}>
              <Text style={styles.Txt} numberOfLines={1} ellipsizeMode="tail">Age:</Text>
              {item.age  != 'undefined'?
              <Text style={styles.detail}>{item.age}</Text>:<Text style={styles.detail}>N/A</Text>}
            </View>
            
            <View style={styles.detailContainer}>
              <Text style={styles.Txt} numberOfLines={1} ellipsizeMode="tail">Sex:</Text>
              {item.sex == 'F'?
              <Text style={styles.detail}>Female</Text>:<Text style={styles.detail}>Male</Text>}
            </View>

              {item.adopted == '0'?
              (<View >
              <Text style={styles.info} color='#4f555b'>  Waiting my forever home </Text>
             </View>)
              :(<View flexDirection='row'marginTop={10}>
              <Text style={styles.info} >  I am happily adopted </Text>
             </View>)}
             </View>
             <View style={styles.card}>
              <Image style={styles.image} source={{uri: this.state.avatar}}/>
              <View style={styles.cardContent}>
                <Text style={styles.name}>{this.state.uname}</Text>
                <TouchableOpacity style={styles.followButton} onPress={()=> this.clickEventListener(item)}>
                  <Text style={styles.followButtonText}>Let's chat</Text>  
                </TouchableOpacity>
              </View>
           </View>
      </View>
      
     }/>
    );
  }
}

const styles = StyleSheet.create({
  header:{
    flex:2,
    backgroundColor: "white",
    height:200,
  },
  avatar: {
    width: 180,
    height: 180,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "#45f74b",
    position:'absolute',
    marginTop:10,
    left:110
  },
  bodyContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding:10,
  },


  detailContainer: { 
    flexDirection: 'row',
    width: 200,
    height:40,
    alignSelf:'center',
   
   
  },
  Txt: {
    justifyContent:"flex-start",
    marginLeft: 15,
    fontWeight: '600',
    color: 'green',
    fontSize: 20,
    width:70,
  },
  detail: {
    justifyContent:"flex-end",
    fontWeight: '600',
    color: '#777',
    fontSize: 18,
  },
  description:{
    left:20,
    marginRight:10,
    alignSelf:'center',
     marginBottom:10,
     fontSize:28,
     color: "#696969",
     fontWeight: "600"
   },
   info:{
     fontSize:24,
     color: "#696969",
     marginTop:10
   },
  body:{
    marginTop:null,
  },
  
  
  cardContent: {
    marginLeft:20,
    marginTop:10
  },
  image:{
    width:80,
    height:80,
    borderRadius:40,
  },
  card:{
    
    justifyContent:'space-evenly',
    alignItems:'center',
    borderRadius:45,
    /* shadowColor: '#00000021',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49, */
   // elevation: 12,
    marginVertical: 10,
    marginHorizontal:30,
    backgroundColor:"white",
    //flexBasis: '46%',
    padding: 10,
    flexDirection:'row'
  },
  name:{
    fontSize:18,
    flex:1,
    alignSelf:'center',
    color:"green",
    fontWeight:'bold'
  },
  
  about:{
    marginHorizontal:10
  },
  followButton: {
    marginTop:10,
    height:35,
    width:100,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius:30,
    backgroundColor: "#45f74b",
  },
  followButtonText:{
    color: "#FFFFFF",
    fontSize:20,
  },

});
 