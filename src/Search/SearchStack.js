import React from 'react'
import {StyleSheet, Image,View, FlatList,TouchableOpacity, Dimensions,Text} from 'react-native'
import axios from 'axios'
import { createStackNavigator } from "@react-navigation/stack";
import { useNavigation } from '@react-navigation/native';
import {AnimalDetail} from  './AnimalDetailStack';

const numColumns=2
const Width= Dimensions.get('window').width

const Stack = createStackNavigator();

 export const SearchStack =()=>{
    
      return(
          <Stack.Navigator>
              <Stack.Screen name= "Animals" component={AnimalPosts} options={{header: ()=>null}} ></Stack.Screen>
              <Stack.Screen name= "Animal Details"  options={{header: ()=>null}} component={AnimalDetail}></Stack.Screen>
          </Stack.Navigator>
      );
 }
 
//create component
export default function(props) {
    const navigation = useNavigation();
    return <MyBackButton {...props} navigation={navigation} />;
  }
export  class AnimalPosts extends React.Component {

    constructor()
    {
        super()
        this.state={
            isRefresh:false,
            data:'',
           
        }
        console.disableYellowBox=true
    }
   componentDidMount(){
       this.makeRequest()
   }
    makeRequest = ()=>{
        axios({method:'GET',
       url:'http://10.0.2.2:8000/api/PetAndUser'})
        .then(res=>{
            console.log('res-->',res.data)
            this.setState({
                data: [...this.state.data,...res.data]}) 
            console.log('dataaa->',this.state.data)
        }).catch(function (error) {
           
           if (error.response) {
             console.log(error.response.data);
             console.log(error.response.status);
             console.log(error.response.headers);
           }
         })
         
    }
    refreshing =()=>{this.setState({isRefresh: true})
        axios({method:'GET',
       url:'http://10.0.2.2:8000/api/PetAndUser'})
        .then(res=>{
            console.log('res-->',res.data)
            this.setState({
                isRefresh: false,
                data: [...this.state.data,...res.data]}) 
            console.log('dataaa->',this.state.data)
        }).catch(function (error) {
            this.setState({isRefresh: false})
           if (error.response) {
             console.log(error.response.data);
             console.log(error.response.status);
             console.log(error.response.headers);
           }
         })
    }
     
    formatData=(dataList,numColumns)=>{
        const totalRows=Math.floor(dataList.length / numColumns)
        let totalLastRow =dataList.length - (totalRows * numColumns)
         while(totalLastRow !==0 && totalLastRow !== numColumns)
         {
             dataList.push({key:'blank',empty:true})
             totalLastRow++
         }
         return dataList
    }
    
    
    render(){
       const { navigation } = this.props;
        return(
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.headerTitle}>Animal Posts</Text>
            </View>
                <FlatList data={this.formatData(this.state.data, numColumns)}
                onRefresh={this.refreshing}
                refreshing={this.state.isRefresh}
                 KeyExtractor={(item,index)=>index.ToString()}
                numColumns={numColumns}
                renderItem={({item})=>
              { if (item.empty)
               {
                   return  ( <View style={styles.invisibleItem}/>)
                } 
               
            return(<View style={styles.itemStyle}>
                <TouchableOpacity style={styles.card} onPress={()=>{this.props.navigation.navigate("Animal Details",{aID :item.id,uname:item.name, avatar: item.avatar});}} >
                    <Image style={styles.pictureItem} source={{uri:item.animal_pic}}/>
                <View style={styles.cardFooter}>
                  <View style={{alignItems:"center", justifyContent:"center"}}>
                    <Text style={styles.description} numberOfLines={2}>{item.description}</Text>
                  </View>
                </View>
                </TouchableOpacity>
                </View>
            )
            }
        }
                />
            </View>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        flex:1,
    },
    header: {
        paddingTop: 14,
        paddingBottom: 16,
        backgroundColor: "#FFF",
        alignItems: "center",
        justifyContent: "center",
        borderBottomWidth: 1,
        borderBottomColor: "#EBECF4",
        shadowColor: "#454D65",
        shadowOffset: { height: 5 },
        shadowRadius: 15,
        shadowOpacity: 0.2,
        zIndex: 10
    },
    headerTitle: {
        fontSize: 20,
        color:'#45f74b',
        fontWeight: "700",
       
    },
    invisibleItem:{
        alignItems:'center',
        justifyContent:'center',
        height:100,
        flex:1,
        margin:1,
        height: Width/numColumns,
        backgroundColor:'transparent'
    },
    itemStyle:{
        alignItems:'center',
        justifyContent:'center',
        flex:1,
        },
    itemText:{
        color:'red',
        fontSize:50
    },
    card:{
        flex:1,
         alignItems:'center',
         justifyContent:'center',
         paddingTop:5,
         shadowColor: '#00000021',
         shadowOffset: {
           width: 4,
           height: 6,},
         shadowOpacity: 0.37,
         shadowRadius: 7.49,
         elevation: 12,
         marginVertical: 5,
         backgroundColor:"white",
         marginHorizontal: 5,
         width:200,
       },
 
  cardFooter: {flex:2,
         paddingVertical: 17,
         paddingHorizontal: 16,
         borderTopLeftRadius: 1,
         borderTopRightRadius: 1,
         flexDirection: 'row',
         alignItems:"center", 
         justifyContent:"center"
       },
    
    
    pictureItem:{
        borderRadius: 53,
        borderWidth: 4,
        borderColor: "#45f74b",
        width: 180,
        height: 180
    }, 
    description:{
        fontSize:18,
        flex:1,
        alignSelf:'center',
        color:"green",
        fontWeight:'bold'
      },

      
})  

