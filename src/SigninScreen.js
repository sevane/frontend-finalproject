
import * as React from 'react';
import { Text, TextInput, View , StyleSheet,TouchableHighlight} from 'react-native';
import {AuthContext} from './utils.js'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'


export function SignInScreen({navigation}) {
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');
  
  const { signIn } = React.useContext(AuthContext);
  return (
  <View style={styles.container}>
  <View style={styles.inputContainer}>
  <TextInput style={styles.inputs}
  placeholder="Email"
  keyboardType="email-address"
  underlineColorAndroid='transparent'
  value={username}
  onChangeText={setUsername}
  />
  </View>
  <View style={styles.inputContainer}>
  <TextInput style={styles.inputs}
  placeholder="Password"
  secureTextEntry={true}
  underlineColorAndroid='transparent'
  value={password}
  onChangeText={setPassword}
  />
  </View>
  <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={() => signIn({ username, password })} >
  <Text style={styles.loginText}>Login</Text>
  </TouchableHighlight>
  <TouchableHighlight style={styles.buttonContainer} onPress={() => navigation.push("CreateAccount", )}>
  <Text>Register</Text>
  </TouchableHighlight>
  </View>
  );
  }

  const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#DCDCDC',
      },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginVertical: 10,
    borderRadius: 5
  }
  ,
  inputContainer: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:30,
      borderBottomWidth: 1,
      width:250,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#FFFFFF',
      flex:1,
  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  loginButton: {
    backgroundColor: "#45f74b",
  },
  loginText: {
    color: 'white',
  }
  });