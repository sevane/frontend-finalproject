import React from 'react';
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {View, Text } from "react-native";
import { HomeStack } from './HomeStack';
//import { ProfileStack } from './ProfileStack';
import {NavigationContainer} from "@react-navigation/native";
//import Ionicons from 'react-native-vector-icons/Ionicons'; 
import { AuthContext } from "./src2/context";
import {
  SignIn,
  CreateAccount,
  Search,
  Home,
  Details,
  Search2,
  Profile,
  Splash
} from "./src2/Screens";
const Tabs = createBottomTabNavigator();

function Home() { 
    return(
        <View style ={{
            flex: 1, alignItems: "center",
            justifyContent: "center"
        }}>
            <Text>I am Home page</Text>

        </View>
    );
}
function Search() {
    return(
        <View style ={{
            flex: 1, alignItems: "center",
            justifyContent: "center"
        }}>
            <Text>I am Search page</Text>

        </View>
    );
}
function SOS() {
    return(
        <View style ={{
            flex: 1, alignItems: "center",
            justifyContent: "center"
        }}>
            <Text>I am SOS page</Text>

        </View>
    );
}
function Profile() {
    return(
        
        <View style ={{
            flex: 1, alignItems: "center",
            justifyContent: "center"
        }}>
            <Text>I am Profile page</Text>

        </View>
    );
}
 const Tab= ({ }) =>
{
    return(

        <Tabs.Navigator>
            <Tabs.Screen name='SOS' component={SOS}/>
            <Tabs.Screen name='Profile' component={Profile}/>
            <Tabs.Screen name='Home' component={HomeStack}/>
            <Tabs.Screen name='Search' component={Search}/>
        </Tabs.Navigator>
       
    );
}
const AuthStack = createStackNavigator();
const AuthStackScreen = () => (
  <AuthStack.Navigator>
    <AuthStack.Screen
      name="SignIn"
      component={SignIn}
      options={{ title: "Sign In" }}
    />
    <AuthStack.Screen
      name="CreateAccount"
      component={CreateAccount}
      options={{ title: "Create Account" }}
    />
  </AuthStack.Navigator>
);



const RootStack = createStackNavigator();
const RootStackScreen = ({ user }) => (
  <RootStack.Navigator headerMode="none">
    {user ? (
      <RootStack.Screen
        name="App"
        component={Tab}
        options={{
          animationEnabled: false
        }}
      />
    ) : (
      <RootStack.Screen
        name="Auth"
        component={AuthStackScreen}
        options={{
          animationEnabled: false
        }}
      />
    )}
  </RootStack.Navigator>
);

export default Auth =() => {
  const [isLoading, setIsLoading] = React.useState(true);
  const [user, setUser] = React.useState(null);

  const authContext = React.useMemo(() => {
    return {
      signIn: () => {
        setIsLoading(false);
        setUser("asdf");
      },
      signUp: () => {
        setIsLoading(false);
        setUser("asdf");
      },
      signOut: () => {
        setIsLoading(false);
        setUser(null);
      }
    };
  }, []);

  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 1000);
  }, []);

  if (isLoading) {
    return <Splash />;
  }

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer >
        <RootStackScreen user={user} />
      </NavigationContainer>
    </AuthContext.Provider>
  );
};