import React from 'react'
import { GiftedChat } from 'react-native-gifted-chat'
import firebase from 'firebase'
import {Fire} from "./Fire"
export class Example extends React.Component {
    constructor() {super()
        if(!firebase.apps.length)
            {var firebaseConfig = {
                apiKey: "AIzaSyDTPf6CZsG2LmoD9PsknjApu3TGf1SBupU",
                authDomain: "myreact-d03d4.firebaseapp.com",
                databaseURL: "https://myreact-d03d4.firebaseio.com",
                projectId: "myreact-d03d4",
                storageBucket: "myreact-d03d4.appspot.com",
                messagingSenderId: "386883955873",
                appId: "1:386883955873:web:03e26a650204ba6b4a94e0",
                measurementId: "G-GYGFWBCCT3"
              };
              // Initialize Firebase
              firebase.initializeApp(firebaseConfig);}
        
      }
    
      
        
    
  state = {
    messages: [],
    
  }
  componentDidMount() {
    const u = (firebase.auth().currentUser || {}).uid
    console.log("u---->",u)
    this.setState({
      messages: [],
    })
  }
  

   onSend(messages = []) {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }))

    Fire.shared.send
  } 
  parse = snapshot => {
    const { timestamp: numberStamp, text, user } = snapshot.val();
    const { key: _id } = snapshot;
    const timestamp = new Date(numberStamp);
    const message = {
      _id,
      timestamp,
      text,
      user,
    };
    return message;
  };

  on = callback =>
    this.ref
      .limitToLast(20)
      .on('child_added', snapshot => callback(this.parse(snapshot)));

  get timestamp() {
    return firebase.database.ServerValue.TIMESTAMP;
  }
  // send the message to the Backend
  send ( messages=[]) {
    for (let i = 0; i < messages.length; i++) {
      const { text, user } = messages[i];
      const message = {
        text,
        user,
        timestamp: this.timestamp,
      };
      this.append(message);
    }
  };
  get ref() { console.log("ref--->",firebase.database().ref('messages'))
    return firebase.database().ref('messages');
  }
  append = message => this.ref.push(message);
  
  render() {
    return (
      <GiftedChat
        messages={this.state.messages}
        onSend={messages => this.send(messages)}
        user={{
          _id: (firebase.auth().currentUser || {}).uid,
        }}
      />
    )
  }
}