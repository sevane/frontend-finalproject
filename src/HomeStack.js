
import React from 'react';
import {View, Text,StyleSheet, FlatList,Image, Dimensions, ActivityIndicator, Button} from "react-native"
import axios from 'axios'
import Icon from 'react-native-vector-icons/Ionicons'
import { createStackNavigator } from "@react-navigation/stack";
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
//import {ChatStack} from "./ChatStack"
import {ChatScreen} from "./ChatScreen"
 const Stack = createStackNavigator();

 export const HomeStack =()=>{
    
      return(
          <Stack.Navigator>
              <Stack.Screen name= "Posts" options={{header: ()=>null}} component={Posts}></Stack.Screen>
              <Stack.Screen name= "Comments"  component={Comments}></Stack.Screen>
              <Stack.Screen name= "ChatStack"  component={ChatScreen} options={({ route }) => ({ title: route.params.fname })}></Stack.Screen>
          </Stack.Navigator>
      );
 }
 function Comments (){
    return(<View style ={{
        flex: 1, alignItems: "center",
        justifyContent: "center"
                        }}>
        <Text>this is Comment</Text>
         </View>);
}
function Chat (){
    return(<View style ={{
        flex: 1, alignItems: "center",
        justifyContent: "center"
                        }}>
        <Text>this is Hello</Text>
         </View>);
}
 
//create component
export default function(props) {
    const navigation = useNavigation();
    return <MyBackButton {...props} navigation={navigation} />;
  }
export  class Posts extends React.Component {

     
 constructor()
 {
     super()
     this.state={
         isLoading: true,
         data:'',
         isRefresh:false,
         uid:''
     }
     console.disableYellowBox=true
 }
componentDidMount(){
    AsyncStorage.getItem('userID').then(value=>
        {   this.setState({uid: value}) })
    this.makeRequest()
}
 makeRequest = ()=>{
     axios({method:'GET',
    url:'http://10.0.2.2:8000/api/PostAndUser'})
     .then(res=>{
         this.setState({
             isLoading:false,
             data: [...this.state.data,...res.data]
         })
     }).catch(function (error) {
        this.setState({
            isLoading:false
        })
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        }
      })
 }
refreshing=()=>{
    this.setState({isRefresh: true})
    axios({method:'GET',
    url:'http://10.0.2.2:8000/api/PostAndUser'})
     .then(res=>{
         this.setState({
             isRefresh:false,
             data: [...this.state.data,...res.data]
         })
         //console.log("response-->",res)
     }).catch(function (error) {
        this.setState({
            isRefresh:false
        })
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        }
      })
}

 renderFooter=()=>{
     return(
         <View>
             <ActivityIndicator animating size="large"/>
         </View>
     )
 } 

 render(){
    console.log('render code',this.state.data)
    if(this.state.isLoading){
        return(
            <View style={styles.Load}>
                <ActivityIndicator animating size="large"/>
            </View>
        )
    }
  
   const { navigation } = this.props;
    return(
        <View style={styles.container}>
            <View style={styles.header}>
                    <Text style={styles.headerTitle}>ALIF</Text>
            </View>
           <FlatList data={this.state.data} 
                            keyExtractor={item=>JSON.stringify(item.id)}
                           // onEndReached={this.handleMore}
                            ListFooterComponent={this.renderFooter}
                            onEndReachedThreshold={10}
                            onRefresh={this.refreshing}
                            refreshing={this.state.isRefresh}
                            renderItem={({item})=>
                                            <View style={styles.feedItem}>
                                            <Image source ={{uri: item.avatar}} style={styles.avatar}/>
                                            <View style={{flex:1}}>
                                                <View style={{ flexDirection: 'row', justifyContent:"space-between", alignItems:"center"}}>
                                                    <View>
                                                    <Text style ={styles.name}>{item.name}</Text>
                                                    </View>
                            
                                                </View>
                                                <View  style={{justifyContent:'flex-start',alignItems:'flex-start'}}>
                                                <Text style={styles.Title}>{item.title}</Text>
                                                <Text style={styles.post}>{item.content}</Text>
                                                </View>
                                              <View  style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                                                <Image source={{uri: item.picture}} style={styles.postImage} ></Image>
                                               </View>
                                                <View style={{flexDirection:'row'}}>
                                                    
                                                    <Icon.Button name="ios-heart" size={24}  backgroundColor="transparent"  color="#73788B"  ></Icon.Button>
                                                    <Icon.Button name="ios-create" size={24} backgroundColor="transparent"  color="#73788B" onPress={()=>{navigation.navigate("Comments");}} ></Icon.Button>
                                                    {item.user_id == this.state.uid?
                                                     <Text> </Text>:<Icon.Button name="ios-chatbubbles" size={24} backgroundColor="transparent"  color="#73788B" onPress={()=>{this.props.navigation.navigate("ChatStack", { fid: item.user_id, uid:this.state.uid,
                                                        fname:item.name });}} ></Icon.Button>}
                                                </View>
                                            </View>
                                        </View>
                            }/>
        </View>
    )
}




}


const styles=StyleSheet.create({
    container:{
        flex: 1, 
       
    },
    header: {
       paddingTop: 14,
        paddingBottom: 16,
        backgroundColor: "#FFF",
        alignItems: "center",
        justifyContent: "center",
        borderBottomWidth: 1,
        borderBottomColor: "#EBECF4",
        shadowColor: "#454D65",
        shadowOffset: { height: 5 },
        shadowRadius: 15,
        shadowOpacity: 0.2,
        zIndex: 10
    },
    headerTitle: {
        fontSize: 20,
        color:'#45f74b',
        fontWeight: "700",
       
    },
    
    name:{
        fontSize:18,
        color:'green',
        marginTop:4
    },
    Load:{
        flex:1,
        padding:20,
        alignItems:'center',
        justifyContent:'center'
    },
    avatar:{
        width:40,
        height:40,
        borderRadius:20,
        marginRight:16

    },
    Title:{
        marginTop:16,
        fontSize:16,
        color: "#14ae76"

    },
    post:{
        right:25,
        marginTop:16,
        fontSize:14,
        color: "#838899"

    },
    postImage:{
        //flex:1,
        width:440,
        height:200,
        //borderRadius:5,
        marginVertical:16,
       right: 40
      // left:30
    },
    feedItem:{
        backgroundColor:"#FFF",
        borderRadius:5,
        padding:8,
        flexDirection: "row",
        marginVertical:8
    }
    

})
