import React from 'react'
import {TouchableOpacity, FlatList,TextInput,View, StyleSheet,Text,SafeAreaView} from "react-native"
import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'firebase'
import Icon from 'react-native-vector-icons/Ionicons'
import {Fire} from './Fire'
export class ChatScreen extends React.Component{
    constructor(props){
        super(props)
        this.state={
            textMessage:'',
            uid:props.route.params.uid,
            fname: props.route.params.fname,
            fid:props.route.params.fid,
            messageList:[]

        }
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(input){
        this.setState({textMessage: input })
    }
    componentDidMount(){
            firebase.database().ref('messages').child(this.state.uid).child(this.state.fid)
            .on('child_added',(value)=>{
            this.setState((prevState)=>{
                return { messageList: [...prevState.messageList, value.val()]}
            })
        }) 
      }

    sendMessage = async ()=>{
        console.log("textmsg-->",this.state.textMessage)
    //  const {fid, fname}= this.props.route.params
       console.log("user id--->", this.state.uid)
       console.log("fid--->", this.state.fid)
       console.log("name--->", this.state.fname)
        if(this.state.textMessage.length > 0)
        { 
            let msgId =  firebase.database().ref('messages').child(this.state.uid).child(this.state.fid).push().key ;
            let updates ={};
            let message ={
                message: this.state.textMessage,
                time: firebase.database.ServerValue.TIMESTAMP,
                from: this.state.uid}
            

                updates['messages/'+this.state.uid+'/'+this.state.fid+'/'+msgId]=message
                updates['messages/'+this.state.fid+'/'+this.state.uid+'/'+msgId]=message

                firebase.database().ref().update(updates);
                this.setState({textMessage:''})
            }
        
    }
    convertTime =(time)=>{
        let d =new Date(time)
        let c = new Date();
        let result =(d.getHours()<10 ? '0':'') + d.getHours() + ':';
        result += (d.getMinutes()<10? '0': '')+ d.getMinutes();
        if(c.getDay() != d.getDay()){
            result = d.getDay() + ' ' + d.getMonth() + ' ' + result
        }
        return result
    }
renderItem=({item})=>{
    return(
       
            <View style={{flexDirection:'row', maxWidth:250, padding:10, borderRadius:300, 
            alignSelf: item.from == this.state.uid ? 'flex-end':'flex-start',
            backgroundColor: item.from == this.state.uid ? '#45f74b':'white',
            borderRadius: 5,
            marginTop: 10}}>
            <Text style={{color:'grey', padding:7, fontSize:16}}>{item.message}</Text>
            <Text style={{color:'grey', padding:3, fontSize:12}}>{this.convertTime(item.time)}</Text>
            </View>
      
    )
}
    render(){
        
        return(
           <View style={{flex:1}}>
               <FlatList style={{paddingHorizontal:17}}
                data={this.state.messageList}
               renderItem={this.renderItem}
               KeyExtractor={(item,index)=>index.toString()}/>

               <View style={styles.footer}>
               <View style={styles.inputContainer}>
                    <TextInput style={styles.inputs}
                        placeholder="Write a message..."
                        underlineColorAndroid='transparent'
                        defaultValue={this.state.textMessage}
                        onChangeText={this.handleChange}/>
                </View>
                <TouchableOpacity style={styles.btnSend} onPress={this.sendMessage}>
                <Icon name="md-send"style={styles.iconSend} size={32} color="#45f74b"></Icon>
                </TouchableOpacity>
                </View>
                </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:"center",
        alignItems:"center",
        backgroundColor:"yellow"
    },
    input:{
        padding : 10,
        borderWidth:1,
        borderColor:'#ccc',
        width:'90%',
        marginBottom:10,
        borderRadius:5

    },
    btnText:{
        color:"green",
        fontSize:20
    },
    footer:{
        flexDirection:'row',
        height:60,
        backgroundColor: '#eeeeee',
        paddingHorizontal: 10,
        padding: 5
    },
    btnSend:{
        backgroundColor:"transparent",
        width:40,
        height:40,
        borderRadius:360,
        alignItems:'center',
        justifyContent:'center',
      },
      iconSend:{
       paddingTop:10,
        alignSelf:'center',
      },
      inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#FFFFFF',
        borderRadius:30,
        borderBottomWidth: 1,
        height:50,
        flexDirection: 'row',
        alignItems:'center',
        flex:1,
        marginRight:10,
      },
      inputs:{
          fontSize:20,
        height:40,
        marginLeft:16,
        borderBottomColor: '#FFFFFF',
        flex:1,
      },
      item:{
          marginVertical:14,
          flex:1,
          flexDirection:'row',
          borderRadius:300,
          padding:5
      }
})