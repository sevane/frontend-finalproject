
import * as React from 'react';
import { Text, TextInput, View , StyleSheet,TouchableHighlight} from 'react-native';
import {AuthContext} from './utils'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'


export function CreateAccount (){
   
    const [name, setName] = React.useState('');
    const [address, setAddress] = React.useState('');
    const [phone, setPhone] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [cpassword, setCPassword] = React.useState('');

const { signUp } = React.useContext(AuthContext);

return (
<KeyboardAwareScrollView  >
<View style={styles.container}>
 <View style={styles.inputContainer}>
<TextInput style={styles.inputs}
  placeholder="Name"
  underlineColorAndroid='transparent'
  value={name}
  onChangeText={setName}
/>
</View>
<View style={styles.inputContainer}>
<TextInput style={styles.inputs}
 placeholder="Phone"
 keyboardType="phone-pad"
 underlineColorAndroid='transparent'
 value={phone}
 onChangeText={setPhone}
/>
</View>
<View style={styles.inputContainer}>
<TextInput style={styles.inputs}
 placeholder="Address"
 underlineColorAndroid='transparent'
 value={address}
 onChangeText={setAddress}
/>
</View>
<View style={styles.inputContainer}>
<TextInput style={styles.inputs}
  placeholder="Email"
  keyboardType="email-address"
  underlineColorAndroid='transparent'
  value={email}
  onChangeText={setEmail}
/>
</View>
<View style={styles.inputContainer}>
<TextInput style={styles.inputs}
 placeholder="Password"
 secureTextEntry={true}
 underlineColorAndroid='transparent'
 value={password}
 onChangeText={setPassword}
/>
</View>
<View style={styles.inputContainer}>
<TextInput style={styles.inputs}
 placeholder="Confirm Password"
 secureTextEntry={true}
 underlineColorAndroid='transparent'
 value={cpassword}
 onChangeText={setCPassword}
/>
</View>
   <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={() => signUp({  name,email,password,cpassword,phone,address})} >
   <Text style={styles.loginText}>Register</Text>
   </TouchableHighlight>

</View></KeyboardAwareScrollView>


);
}


const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#DCDCDC',
      },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginVertical: 10,
    borderRadius: 5
  }
  ,
  inputContainer: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:30,
      borderBottomWidth: 1,
      width:250,
      height:45,
      marginTop:20,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#FFFFFF',
      flex:1,
  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  loginButton: {
    backgroundColor: "#45f74b",
  },
  loginText: {
    color: 'white',
  }
  });