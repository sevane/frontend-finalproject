//import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import * as React from 'react';
import{View,Text,TouchableHighlight,TextInput, TouchableOpacity,ScrollView, StatusBar, StyleSheet, FlatList, Dimensions, Image, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import ImagePicker from 'react-native-image-crop-picker';
import {ActionSheet, Root} from 'native-base'
import axios from 'axios'
import {Animal} from "./AnimalStack"
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import { useNavigation } from '@react-navigation/native';
const Tab = createBottomTabNavigator();

export default function(props) {
  const navigation = useNavigation();
  return <MyBackButton {...props} navigation={navigation} />;
}
export const PostStack =()=>{
    
    return(
    <Tab.Navigator style={{justifyContent:'center', alignItems:'center', fontSize:16}} tabBarOptions={{
      activeTintColor: '#45f74b',
      inactiveTintColor: 'gray'}}>
      <Tab.Screen name="Post" component={Post} />
      <Tab.Screen name="Animal" component={Animal} />
    </Tab.Navigator>
  );
}

export class Post extends React.Component{

  constructor(){
    super()
    this.state={
      title:"",
      content:"",
      image:'',
      userid:''
    }
    this.handleChangeTitle = this.handleChangeTitle.bind(this)
    this.handleChangeContent = this.handleChangeContent.bind(this)
  }
//input change handlers
  handleChangeTitle(inputText){
    this.setState({
      title: inputText
    })
  }

  handleChangeContent(inputText){
    this.setState({
      content: inputText
    })
  }

//ActionSheet handler 
  onClickAddImage=()=>{
    const btn = ['Take Photo','Choose Photo Library','Cancel'];
    ActionSheet.show(
      {options: btn, cancelButtonIndex:2, title:'Select a photo'},
     buttonIndex=>{
       switch(buttonIndex){
         case 0:
           this.takePhotoFromCamera();
           break;
           case 1:
            this.choosePhotoFromLibrary();
             break;
             default:
               break;
       }}
    )}

//image picker function to chose photo from library
  choosePhotoFromLibrary(){
    ImagePicker.openPicker({
      Width: 300,
      Height: 400,
      cropping: true,
    
    }).then(image => {
      console.log("image--->",image);
      this.setState({image: image.path})
    });
  }

  //image picker function to take a photo
  takePhotoFromCamera(){
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      console.log("image--->",image);
      this.setState({image: image.path})
    });
  }

//fetch the user id from async storage and store it
  componentDidMount(){
    let UID
  AsyncStorage.getItem('userID').then((response)=>{
        UID = response;
        this.setState({
          userid: UID
        })
        console.log("user ID inside Add Post:",this.state.userid)
  
  })}

  //add post (title,content, userid, picture to backend)
  onClickAddPost=()=>{
    console.log("titleee-->",this.state.title)
    console.log("con-->",this.state.content)
    console.log("img-->",this.state.image)
    
    //pass params as form data
      var data = new FormData();
     data.append('title',this.state.title)
       data.append('content',this.state.content)
      data.append('picture',this.state.image,
                             );
      data.append('user_id',this.state.userid)
    
     axios.post("http://10.0.2.2:8000/api/store",data)
    .then(response=>{console.log("response-->",response)
    this.props.navigation.navigate("Posts")})
    .catch(function(error) {console.log("error--->",error)});
  }


  render(){
    return(
      <KeyboardAwareScrollView style={{flex:1}} >
       <View style={styles.container}>
        <StatusBar barStyle="dark-content"/>
        <ScrollView contentContainerStyle={{flex: 1, persistentScrollbar: true}}>
          <Root>
            <TouchableOpacity onPress={()=> this.onClickAddImage()}
            style={styles.avatar}>
              {this.state.image != '' && ( 
                <Image source={{uri: this.state.image}}
                style={{height:175, width:192, borderRadius:61}}/>
                )} 

                </TouchableOpacity>
                </Root>
                </ScrollView> 
               
                <View style={styles.inputContainer} marginTop={25}>
                <TextInput style={styles.inputs}
                  placeholder="Title"
                  keyboardType="default"
                  underlineColorAndroid='transparent'
                  defaultValue={this.state.title}
                  onChangeText={this.handleChangeTitle}/>
                </View>
                <View style={styles.inputContent}>
                <TextInput style={styles.inputs}
                  placeholder="Content"
                  multiline={true}
                  numberOfLines={4}
                  underlineColorAndroid='transparent'
                  defaultValue={this.state.content}
                  onChangeText={this.handleChangeContent}
                />
                </View>
                <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={this.onClickAddPost} >
                <Text style={styles.buttonText}>Add Post</Text>
                </TouchableHighlight>
     </View></KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
       flex: 1,
       justifyContent: 'center',
       alignItems: 'center',
       backgroundColor: '#DCDCDC',
     },
 button: {
   paddingHorizontal: 20,
   paddingVertical: 10,
   marginVertical: 10,
   borderRadius: 5
 },
 inputContainer: {
     borderBottomColor: '#F5FCFF',
     backgroundColor: '#FFFFFF',
     borderRadius:30,
     borderBottomWidth: 1,
     width:250,
     height:45,
     marginTop:250,
     marginBottom:50,
     flexDirection: 'row',
     alignItems:'center'
 },
 inputContent: {
   borderBottomColor: '#F5FCFF',
   backgroundColor: '#FFFFFF',
   borderRadius:30,
   borderBottomWidth: 1,
   width:250,
   height:150,
   marginBottom:20,
   flexDirection: 'row',
   alignItems:'center'
 },
 inputs:{
     height:45,
     marginLeft:16,
     borderBottomColor: '#FFFFFF',
     flex:1,
    
 },
 content:{
  height:45,
  marginLeft:16,
  borderBottomColor: '#FFFFFF',
  flex:1,
},
 content:{
   height:240,
   marginLeft:130,
   borderBottomColor: '#FFFFFF',
   flex:1,
 },
 inputIcon:{
   width:30,
   height:30,
   marginLeft:15,
   justifyContent: 'center'
 },
 buttonContainer: {
   height:45,
   flexDirection: 'row',
   justifyContent: 'center',
   alignItems: 'center',
   marginBottom:20,
   width:250,
   borderRadius:30,
 },
 loginButton: {
   backgroundColor: "#45f74b",
 },
 buttonText: {
   color: 'white',
 },
 avatar: {
  width: 200,
  height: 180,
   borderRadius: 63,
   borderWidth: 4,
   borderColor: "#45f74b",
   justifyContent: "center",
   backgroundColor:"white",
   marginTop:10,
   marginBottom:20
   
 },
 });