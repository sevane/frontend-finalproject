import * as React from 'react';
import{View,Text,TouchableHighlight,TextInput, TouchableOpacity,ScrollView, StatusBar,KeyboardAvoidingView,StyleSheet, FlatList, Dimensions, Image, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-crop-picker';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button'
import {ActionSheet, Root} from 'native-base'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import { useNavigation } from '@react-navigation/native';
import axios from 'axios'

 var sex =[
   {label: 'Male            ', value:'M'},{label: 'Female', value:'F'}
 ]

 export default function(props) {
  const navigation = useNavigation();
  return <MyBackButton {...props} navigation={navigation} />;
}

export class Animal extends React.Component{

    constructor(){
      super()
      this.state={
        type:"",
        breed:"",
        image:'',
        userid:'',
        sex:'',
        age:'',
        description:''
        
      }
      
      this.handleChangeAge = this.handleChangeAge.bind(this)
      this.handleChangeBreed = this.handleChangeBreed.bind(this)
      this.handleChangeType = this.handleChangeType.bind(this)
      this.handleChangeDescription = this.handleChangeDescription.bind(this)
    }
  //input change handlers
  handleChangeAge(inputText){this.setState({age: inputText})}
  handleChangeBreed(inputText){this.setState({breed: inputText})}
  handleChangeType(inputText){this.setState({type: inputText})}
  handleChangeDescription(inputText){this.setState({description: inputText})}
initialState(){
  this.setState({type:"",
  breed:"",
  image:'',
  userid:'',
  sex:'',
  age:'',
  description:''})
}

  //ActionSheet handler 
    onClickAddImage=()=>{
      const btn = ['Take Photo','Choose Photo Library','Cancel'];
      ActionSheet.show(
        {options: btn, cancelButtonIndex:2, title:'Select a photo'},
       buttonIndex=>{
         switch(buttonIndex){
           case 0:
             this.takePhotoFromCamera();
             break;
             case 1:
              this.choosePhotoFromLibrary();
               break;
               default:
                 break;
         }}
      )}
  
  //image picker function to chose photo from library
    choosePhotoFromLibrary(){
      ImagePicker.openPicker({
        Width: 300,
        Height: 400,
        cropping: true,
      
      }).then(image => {
        console.log("image--->",image);
        this.setState({image: image.path})
      });
    }
  
    //image picker function to take a photo
    takePhotoFromCamera(){
      ImagePicker.openCamera({
        width: 300,
        height: 400,
        cropping: true,
      }).then(image => {
        console.log("image--->",image);
        this.setState({image: image.path})
      });
    }
  
  //fetch the user id from async storage and store it
    componentDidMount(){
      let UID
    AsyncStorage.getItem('userID').then((response)=>{
          UID = response;
          this.setState({
            userid: UID
          })
          console.log("user ID inside Add Post:",this.state.userid)
    
    })}
  
    //add post (title,content, userid, picture to backend)
    onClickAddPost=()=>{const { navigation } = this.props;
      console.log("sex-->",this.state.sex)
      console.log("type-->",this.state.content)
      console.log("breed-->",this.state.image)
       
      var data = new FormData();
       data.append('type',this.state.type)
         data.append('breed',this.state.breed)
         data.append('age',this.state.agee)
         data.append('sex',this.state.sex)
         data.append('description',this.state.description)
        data.append('animal_pic',this.state.image,);
        data.append('user_id',this.state.userid)
      
       axios.post("http://10.0.2.2:8000/api/storePet",data)
      .then(//function (response){console.log("response-->",response)}
      response=>{console.log("response-->",response)
      this.props.navigation.navigate("Animals")})
      .catch(function(error) {console.log("error--->",error)});
      

    }
  
  
    render(){
      return(
        <KeyboardAwareScrollView style={{flex:1}} >
         <View style={styles.container}>
          <StatusBar barStyle="dark-content"/>
          <ScrollView contentContainerStyle={{flex: 1, persistentScrollbar: true}}>
            <Root>
              <TouchableOpacity onPress={()=> this.onClickAddImage()}
              style={styles.avatar}>
                {this.state.image != '' ? ( 
                  <Image source={{uri: this.state.image}}
                  style={{height:175, width:192, borderRadius:61}}/>
                  ):<Image source={require ('/home/sefactory-02/MyNewReact/frontend-finalproject/src/assets/imagepetcam.png')}
                  style={{height:175, width:192, borderRadius:61}}/>} 
                  
  
                  </TouchableOpacity>
                  </Root>
                  </ScrollView> 
                  <View style={styles.inputContainer} marginTop={8}>
                  <TextInput style={styles.inputs}
                    placeholder="Type"
                    keyboardType="default"
                    underlineColorAndroid='transparent'
                    defaultValue={this.state.type}
                    onChangeText={this.handleChangeType}/>
                  </View>
                  <View style={styles.inputContainer} marginTop={8}>
                  <TextInput style={styles.inputs}
                    placeholder="Breed"
                    keyboardType="default"
                    underlineColorAndroid='transparent'
                    defaultValue={this.state.breed}
                    onChangeText={this.handleChangeBreed}/>
                  </View>
                  <View style={styles.inputContainer} marginTop={8} >
                  <TextInput style={styles.inputs}
                    placeholder="Age"
                    keyboardType="default"
                    underlineColorAndroid='transparent'
                    defaultValue={this.state.age}
                    onChangeText={this.handleChangeAge}/>
                  </View>
                  <View style={styles.inputContainer} marginTop={8} >
                  <RadioForm radio_props={sex} initial={2} style={{marginLeft:20}}
                  buttonSize={10} buttonOuterSize={20} 
                   selectedButtonColor={'#45f74b'} buttonColor={'grey'}
                  labelHorizontal={true} formHorizontal={true} 
                  onPress={(value)=>{this.setState({sex: value})}} />
                  </View>

                <View style={styles.inputContent} marginTop={8}>
                <TextInput style={styles.description}
                  placeholder="Description"
                  multiline={true}
                  numberOfLines={4}
                  underlineColorAndroid='transparent'
                  defaultValue={this.state.description}
                  onChangeText={this.handleChangeDescription}
                />
                </View>

                  <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={this.onClickAddPost} >
                  <Text style={styles.loginText}>Add Post</Text>
                  </TouchableHighlight>
       </View></KeyboardAwareScrollView>
      );
    }
  }
  
  const styles = StyleSheet.create({
    container: {
         flex: 1,
         justifyContent: 'center',
         alignItems: 'center',
         backgroundColor: '#DCDCDC',
       },
       container2:{
         flex:2,
         justifyContent: "center",
         alignItems:"center",
         backgroundColor: '#DCDCDC',
       },
   button: {
     paddingHorizontal: 20,
     paddingVertical: 10,
     marginVertical: 10,
     borderRadius: 5
   },
   inputContainer: {
       borderBottomColor: '#F5FCFF',
       backgroundColor: '#FFFFFF',
       borderRadius:30,
       borderBottomWidth: 1,
       width:250,
       height:45,
       //marginTop:200,
       //marginBottom:50,
       flexDirection: 'row',
       alignItems:'center'
   },
   inputContent: {
     borderBottomColor: '#F5FCFF',
     backgroundColor: '#FFFFFF',
     borderRadius:30,
     borderBottomWidth: 1,
     width:250,
     height:100,
     marginBottom:8,
     flexDirection: 'row',
     alignItems:'center'
   },
   inputs:{
       height:45,
       marginLeft:16,
       borderBottomColor: '#FFFFFF',
       flex:1,
   },
   description:{
    height:50,
    marginLeft:16,
    borderBottomColor: '#FFFFFF',
    flex:1,
},
   content:{
     height:240,
     marginLeft:16,
     borderBottomColor: '#FFFFFF',
     flex:1,
   },
   inputIcon:{
     width:30,
     height:30,
     marginLeft:15,
     justifyContent: 'center'
   },
   buttonContainer: {
     height:45,
     flexDirection: 'row',
     justifyContent: 'center',
     alignItems: 'center',
     marginBottom:20,
     width:250,
     borderRadius:30,
   },
   loginButton: {
     backgroundColor: "#45f74b",
   },
   loginText: {
     color: 'white',
   },
   avatar: {
     width: 200,
     height: 180,
     borderRadius: 63,
     borderWidth: 4,
     borderColor: "#45f74b",
     justifyContent: "center",
     backgroundColor:"white",
     marginTop:10,
     
   },
   });