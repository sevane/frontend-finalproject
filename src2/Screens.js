import React from "react";
import {StyleSheet, Text,View,TextInput,TouchableHighlight, Button} from 'react-native'
import { AuthContext } from "./context";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#DCDCDC',
      },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginVertical: 10,
    borderRadius: 5
  }
  ,
  inputContainer: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:30,
      borderBottomWidth: 1,
      width:250,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#FFFFFF',
      flex:1,
  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  loginButton: {
    backgroundColor: "#45f74b",
  },
  loginText: {
    color: 'white',
  }
});

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Home = ({ navigation }) => (
  <ScreenContainer>
    <Text>Master List Screen</Text>
    <Button
      title="React Native by Example"
      onPress={() =>
        navigation.push("Details", { name: "React Native by Example " })
      }
    />
    <Button
      title="React Native School"
      onPress={() =>
        navigation.push("Details", { name: "React Native School" })
      }
    />
    <Button title="Drawer" onPress={() => navigation.toggleDrawer()} />
  </ScreenContainer>
);

export const Details = ({ route }) => (
  <ScreenContainer>
    <Text>Details Screen</Text>
    {route.params.name && <Text>{route.params.name}</Text>}
  </ScreenContainer>
);

export const Search = ({ navigation }) => (
  <ScreenContainer>
    <Text>Search Screen</Text>
    <Button title="Search 2" onPress={() => navigation.push("Search2")} />
    <Button
      title="React Native School"
      onPress={() => {
        navigation.navigate("Home", {
          screen: "Details",
          params: { name: "React Native School" }
        });
      }}
    />
  </ScreenContainer>
);

export const Search2 = () => (
  <ScreenContainer>
    <Text>Search2 Screen</Text>
  </ScreenContainer>
);

export const Profile = ({ navigation }) => {
  const { signOut } = React.useContext(AuthContext);

  return (
    <ScreenContainer>
      <Text>Profile Screen</Text>
      <Button title="Drawer" onPress={() => navigation.toggleDrawer()} />
      <Button title="Sign Out" onPress={() => signOut()} />
    </ScreenContainer>
  );
};

export const Splash = () => (
  <ScreenContainer>
    <Text>Loading...</Text>
  </ScreenContainer>
);

/*export const SignIn = ({ navigation }) => {
    
                const [username, setUsername] = React.useState('');
                const [password, setPassword] = React.useState('');
  const { signIn } = React.useContext(AuthContext);
 // const { username, password}= this.state;
  return (
    <ScreenContainer>
      <View style={styles.container}>
            <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
                placeholder="Email"
                value={username}
                keyboardType="email-address"
                underlineColorAndroid='transparent'
                //onChangeText={email => this.setUse({username: email})}
                onChangeText={setUsername} 
                />
          </View>
          
          <View style={styles.inputContainer}>
               <TextInput style={styles.inputs}
                placeholder="Password"
                value={password}
                secureTextEntry={true}
                underlineColorAndroid='transparent'
                //onChangeText={pass => this.setState({password: pass})}
                onChangeText={setPassword}
                />
          </View>
  
          <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={() => signIn()} >
          <Text style={styles.loginText}>Login</Text>
          </TouchableHighlight>
          <TouchableHighlight style={styles.buttonContainer}
        onPress={() => navigation.push("CreateAccount")}>
         <Text>Register</Text>
         </TouchableHighlight>
      </View>
    </ScreenContainer>
  );
};

export const CreateAccount = () => {
    state = {name:"",
            address:"",
            phone:"",
            email:"",
            password:"",
            c_password:""}
  const { signUp } = React.useContext(AuthContext);

  return (
    <ScreenContainer>
    <View style={styles.container}>
            <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
                placeholder="Name"
                keyboardType="name"
                underlineColorAndroid='transparent'
                onChangeText={name => this.setState({name: name})}/>
             </View>
             <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
                placeholder="Phone"
                keyboardType="phone"
                underlineColorAndroid='transparent'
                onChangeText={phone => this.setState({phone: phone})}/>
             </View>
             <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
                placeholder="Address"
                keyboardType="address"
                underlineColorAndroid='transparent'
                onChangeText={address => this.setState({address: address})}/>
             </View>
            <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
                placeholder="Email"
                keyboardType="email-address"
                underlineColorAndroid='transparent'
                onChangeText={email => this.setState({username: email})}/>
             </View>
        
            <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
                placeholder="Password"
                secureTextEntry={true}
                underlineColorAndroid='transparent'
                onChangeText={password => this.setState({password: password})}/>
        </View>
        <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
                placeholder="Confirm Password"
                secureTextEntry={true}
                underlineColorAndroid='transparent'
                onChangeText={c_password => this.setState({c_password: c_password})}/>
        </View>
    
        <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={() => signUp()} >
      <Text style={styles.loginText}>Register</Text>
      </TouchableHighlight>

      </View>
    </ScreenContainer>
    
  );
};*/

export const  SignInScreen = ({navigation}) =>{
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');

  const { signIn } = React.useContext(AuthContext);

  return (
    <View style={styles.container}>
      <View style={styles.inputContainer}>
      <TextInput style={styles.inputs}
        placeholder="Email"
        keyboardType="email-address"
        underlineColorAndroid='transparent'
        value={username}
        onChangeText={setUsername}
      />
      </View>
      <View style={styles.inputContainer}>
      <TextInput style={styles.inputs}
        placeholder="Password"
        secureTextEntry={true}
        underlineColorAndroid='transparent'
        value={password}
        onChangeText={setPassword}
      />
      </View>
      <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={() => signIn({ username, password })} >
          <Text style={styles.loginText}>Login</Text>
          </TouchableHighlight>
          <TouchableHighlight style={styles.buttonContainer} onPress={() => navigation.navigate("CreateAccount")}>
         <Text>Register</Text>
         </TouchableHighlight>
    </View>
  );
}

 export const CreateAccount =() =>{
          const [name, setName] = React.useState('');
          const [address, setAddress] = React.useState('');
          const [phone, setPhone] = React.useState('');
          const [email, setEmail] = React.useState('');
          const [password, setPassword] = React.useState('');
          const [cpassword, setCPassword] = React.useState('');

const { signUp } = React.useContext(AuthContext);

return (
  <ScreenContainer>
  <View style={styles.container}>
          <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
              placeholder="Name"
              keyboardType="name"
              underlineColorAndroid='transparent'
              value={name}
              onChangeText={setName}/>
           </View>
           <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
              placeholder="Phone"
              keyboardType="phone"
              underlineColorAndroid='transparent'
              value={phone}
              onChangeText={setPhone}/>
           </View>
           <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
              placeholder="Address"
              keyboardType="address"
              underlineColorAndroid='transparent'
              value={address}
              onChangeText={setAddress}/>/>
           </View>
          <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
              placeholder="Email"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              value={email}
              onChangeText={setEmail}/>/>
           </View>
      
          <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
              placeholder="Password"
              secureTextEntry={true}
              underlineColorAndroid='transparent'
              value={password}
              onChangeText={setPassword}/>
      </View>
      <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
              placeholder="Confirm Password"
              secureTextEntry={true}
              underlineColorAndroid='transparent'
              value={cpassword}
              onChangeText={setCPassword}/>
      </View>
  
      <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={() => signUp()} >
    <Text style={styles.loginText}>Register</Text>
    </TouchableHighlight>

    </View>
  </ScreenContainer>
  
);
}