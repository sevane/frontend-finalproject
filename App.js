
import * as React from 'react';
import { Button, Text,  View , StyleSheet} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import Ionicons from 'react-native-vector-icons/Ionicons'; 
import { HomeStack } from './src/HomeStack';
import { PostStack } from './src/AddPost/PostStack';
import { ProfileStack } from './src/Profile/ProfileStack';
import {SearchStack} from './src/Search/SearchStack';
import {SplashScreen} from './src/SplashScreen'
import {SignInScreen} from './src/SigninScreen'
import {CreateAccount } from './src/CreateAccount'
import {Sos} from './src/SosStack'
import AsyncStorage from '@react-native-community/async-storage';
import {AuthContext} from './src/utils'
import axios from 'axios'
import firebase from 'firebase'
//const AuthContext = React.createContext();



const Tabs = createBottomTabNavigator();
const Tab= () =>
{
    return(

        <Tabs.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = focused
                ? 'ios-home'
                : 'ios-home';
            } else if (route.name === 'Search') {
              iconName = focused ? 'ios-search' : 'ios-search';
            }else if (route.name === 'SOS') {
              iconName = focused ? 'ios-paw' : 'ios-paw';
            }else if (route.name === 'Profile') {
              iconName = focused ? 'ios-person' : 'ios-person';
            }else if (route.name === 'Add') {
              iconName = focused ? 'ios-add-circle' : 'ios-add-circle';
            }

            // You can return any component that you like here!
            return <Ionicons name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: '#45f74b',
          inactiveTintColor: 'gray',

          showLabel:false,
        }}>
            <Tabs.Screen name='Home' component={HomeStack} />
            <Tabs.Screen name='Search' component={SearchStack}/>
            <Tabs.Screen name='Add' component={PostStack}/>
            <Tabs.Screen name='SOS'  component={Sos}/>
            <Tabs.Screen name='Profile'  options={{ headerTitle: "Profile",
              headerRight: ()=>(
              <Button onPress={()=>alert('this is button!')} title="Menu" color="#fff" />
            )}} component={ProfileStack}/>
        </Tabs.Navigator>
       
    );
}
function HomeScreen() {
  const { signOut } = React.useContext(AuthContext);

  return (
    <View>
      <Text>Signed in!</Text>
      <Button title="Sign out" onPress={signOut} />
      <Text justifyContent="center" color="red">NOT COMPLETE</Text>
    </View>
  );
}


const Stack = createStackNavigator();

export default function App({ navigation }) {
  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
    }
  );
  
  React.useEffect(() => {
    
  
    // Fetch the token from storage then navigate to our appropriate place
      getData = async () => {
        let userToken;
       console.log("inside effect")
        try {
          userToken= await AsyncStorage.getItem('userToken')
        
        } catch(e) {
          // error reading value
        }
      
      // After restoring token, we may need to validate it in production apps

      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
      console.log("token  after", userToken)
      dispatch({ type: 'RESTORE_TOKEN', token: userToken })
    }

    getData()
  }, [])
  

  const authContext = React.useMemo(
    () => ({
      storeData : async token => {
        console.log("before try")
        try {
          console.log("store data 1")
          await AsyncStorage.setItem('userToken', token)
        } catch (e) {
          console.log("store data error",e)
        }
      },
      signIn: async data => {
        // In a production app, we need to send some data (usually username, password) to server and get a token
        // We will also need to handle errors if sign in failed
        // After getting token, we need to persist the token using `AsyncStorage`
        // In the example, we'll use a dummy token
                console.log("Sign in", data);
                  axios.post('http://10.0.2.2:8000/api/login',{
                  email:data.username, password:data.password 
                }).then(res=>
                  {

                  {firebase.auth().signInWithEmailAndPassword(data.username,data.password)
                  .then((value)=>{console.log("chatvalue---->",JSON.stringify(value))
                  console.log("current---------->",JSON.stringify(firebase.auth().currentUser))
                  console.log("authuser------->",(firebase.auth().currentUser || {}).uid)

                   firebase.database().ref('users/'+res.data.data.userID).set ({name: res.data.data.name })})
                  .catch(error=>console.log(error)) }

                  console.log('code response with token--->',res)
                  console.log("res.data.toke--->",res.data.data.token)
                  console.log("res.data.ID--->",res.data.data.userID)
                  let token = res.data.data.token
                  let UID=res.data.data.userID
                  console.log("before try")
                  try {
                    console.log("store data 1")
                     AsyncStorage.setItem('userToken', token)
                     AsyncStorage.setItem('userID', UID.toString())
                     
                  } catch (e) {
                    console.log("store data error",e)
                  }
                   dispatch({ type: 'SIGN_IN', token: 'res.data.data.token' });
                   }).catch(function (error) {
                
                if (error.response) {
                  console.log("error1",error.response.data);
                  console.log("error2",error.response.status);
                  console.log("error3",error.response.headers);
                  alert('Incorrect username or password.')
                }
              })
            console.log(data.username,"------",data.password)


               
      },

      signOut: () => {
         AsyncStorage.setItem('userToken', '')
         AsyncStorage.setItem('userID', '')
        /* firebase.auth().signOut()
        .then(()=>{
          console.log("signed out from firebase")
          
        }) */
        dispatch({ type: 'SIGN_OUT' })},
      signUp: async data => {
        // In a production app, we need to send user data to server and get a token
        // We will also need to handle errors if sign up failed
        // After getting token, we need to persist the token using `AsyncStorage`
        // In the example, we'll use a dummy token
        console.log("Sign up", data);
                  axios.post('http://10.0.2.2:8000/api/register',{ name:data.name,
                  email:data.email, password:data.password, c_password:data.cpassword,
                  phone:data.phone,address:data.address
                }).then(res=>{
                  console.log('code response--->',res)
                  let token = res.data.data.token
                  let UID=res.data.data.userID
                  console.log("before try")
                  try {
                    console.log("store data 1")
                     AsyncStorage.setItem('userToken', token)
                     AsyncStorage.setItem('userID', UID.toString())
                     
                  } catch (e) {
                    console.log("store data error",e)
                  }
                  dispatch({ type: 'SIGN_IN', token: 'res.data.data.token' });
                  
              }).catch(function (error) {
                
                if (error.response) {
                  console.log("error1",error.response.data);
                  console.log("error2",error.response.status);
                  console.log("error3",error.response.headers);
                }
                alert("Incorrect username or password.")
              })
              console.log("reg email--->",data.email)
              /* const displayName= data.name
              firebase.auth().createUserWithEmailAndPassword(data.email,data.password)
              /* .then((userInfo)=>{
                console.log('userInfo----->',userInfo)
                userInfo.user.updateProfile({displayName: displayName.trim()})
                .then(()=>{})
              }) */
              /* .then(authRes =>{
                const userObj={
                  email: authRes.user.email
                };
                console.log("userObj--->",userObj)
                firebase.firestore()
                .collection('users')
                .doc(data.email)
                .set(userObj)
               }) */ 
      },
    }),
    []
  );

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <Stack.Navigator>
          {state.isLoading ? (
            // We haven't finished checking for the token yet
            <Stack.Screen name="Splash" component={SplashScreen} />
          ) : state.userToken == null ? (
            // No token found, user isn't signed in
            <>
            
            <Stack.Screen
              name="SignIn"
              component={SignInScreen}
              options={{
                title: 'Sign in',
            // When logging out, a pop animation feels intuitive
                animationTypeForReplace: state.isSignout ? 'pop' : 'push',
              }} />
            <Stack.Screen name="CreateAccount" component={CreateAccount} />
            </>
            
          ) : (
            // User is signed in
            <Stack.Screen name="App"
            component={Tab}
            options={{
              header: ()=>null,
              animationEnabled: false
            }} token={state.userToken} />
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </AuthContext.Provider>
  );
}
const styles = StyleSheet.create({
  container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#DCDCDC',
    },
button: {
  paddingHorizontal: 20,
  paddingVertical: 10,
  marginVertical: 10,
  borderRadius: 5
}
,
inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius:30,
    borderBottomWidth: 1,
    width:250,
    height:45,
    marginBottom:20,
    flexDirection: 'row',
    alignItems:'center'
},
inputs:{
    height:45,
    marginLeft:16,
    borderBottomColor: '#FFFFFF',
    flex:1,
},
inputIcon:{
  width:30,
  height:30,
  marginLeft:15,
  justifyContent: 'center'
},
buttonContainer: {
  height:45,
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  marginBottom:20,
  width:250,
  borderRadius:30,
},
loginButton: {
  backgroundColor: "#45f74b",
},
loginText: {
  color: 'white',
}
});