
import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity, FlatList,Linking} from 'react-native';
import axios from 'axios'
import Ionicons from 'react-native-vector-icons/Ionicons'


export  class ProfileStack extends Component {
  constructor()
 {
     super()
     this.state={
         isLoading: true,
         data:'',
         
     }
     console.disableYellowBox=true
 }
componentDidMount(){
  //console.log("");
  getData = async () => {
    let userToken;
   console.log("inside profile getdata")
    try {
      console.log("profile get Data inside try")
      userToken= await AsyncStorage.getItem('userToken')
    
    } catch(e) {
      // error reading value
    }
  }
  getData()
    this.makeRequest()
}
  makeRequest = ()=>{
    axios.get('https://5ea428ca270de600164600de.mockapi.io/api/posts/1' )
    
    .then(res=>{
        console.log('code profile response--->',res)
        this.setState({
          data: [res.data.data]
        })
    })
}
Dial=()=>{
  Linking.openURL("tel:70322307")
}
  render() {

    return (
      <FlatList data={this.state.data} 
                            keyExtractor={(item)=>item.first_name}
                            onEndReached={this.makeRequest}
                            onEndReachedThreshold={1}
                            renderItem={
                              ({item})=>
      <View style={styles.container}>
          
          <View style={styles.header}><Image source={require ('./assets/alif.jpg')} 
          style={{width:200, height:200}}/>
          <Image source ={{uri: item.avatar}} style={styles.avatar}/></View>
          <View style={styles.body}>
            <View style={styles.bodyContent}>
            <Text style ={styles.name}>{item.first_name+ " "+ item.last_name}</Text>
              <Text style={styles.info}>UX Designer / Mobile developer</Text>
              <Text style={styles.description}>Lorem ipsum dolor sit amet, saepe sapientem eu nam. Qui ne assum electram expetendis, omittam deseruisse consequuntur ius an,</Text>
              <View style={styles.Card}>
                <Ionicons name="ios-call" size={18} />
              <Text style={styles.CardText} onPress={this.Dial}>70322307</Text>
              
              </View>
              <View style={styles.Card}>
                <Ionicons name="ios-pin" size={18} />
                <Text style={styles.CardText}>Address</Text>
              </View>

              <TouchableOpacity style={styles.buttonContainer}>
                <Text>Edit Profile</Text>  
              </TouchableOpacity>              
              <TouchableOpacity style={styles.buttonContainer}>
                <Text>Add Post</Text> 
              </TouchableOpacity>
            </View>
        </View>
      
      </View>}/>
    );
  }
}

const styles = StyleSheet.create({
  header:{
    flex:2,
    backgroundColor: "white",
    height:200,
  },
  avatar: {
    width: 180,
    height: 180,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "#45f74b",
    marginLeft:120,
    marginBottom:110,
    position:'absolute',
    marginTop:10,
    left:100
  },
  
  body:{
    marginTop:null,
  },
  bodyContent: {
    flex: 1,
    alignItems: 'center',
    padding:10,
  },
  name:{
    fontSize:28,
    color: "#696969",
    fontWeight: "600"
  },
  info:{
    fontSize:16,
    color: "#696969",
    marginTop:10
  },
  description:{
    fontSize:16,
    color: "#696969",
    marginTop:10,
    textAlign: 'center',
    marginBottom:10
  },
  buttonContainer: {
    marginTop:10,
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    
    width:250,
    borderRadius:30,
    backgroundColor: "#45f74b",
  },
  Card:{
    marginTop:5,
    height:35,
    flexDirection: 'row',
    alignItems: 'center',
    width:250,
    backgroundColor:"white",
  },
  CardText:{
    fontSize:18,
    left:20,
    color:"blue"
  },
  button: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 10
  },

});
 