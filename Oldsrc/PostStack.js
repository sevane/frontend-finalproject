//import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import * as React from 'react';
import{View,Text,TouchableHighlight,TextInput, StyleSheet, FlatList, Dimensions, Image, Alert } from 'react-native'
//import AsyncStorage from '@react-native-community/async-storage';
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import ImagePicker from 'react-native-image-crop-picker';
import {ActionSheet, Root} from 'native-base'
const Tab = createBottomTabNavigator();

//const Tab = createMaterialTopTabNavigator();
export const PostStack =()=>{
    
    return(
    <Tab.Navigator>
      <Tab.Screen name="Post" component={Post} />
      <Tab.Screen name="Animal" component={Animal} />
    </Tab.Navigator>
  );
}

/* 
function Post (){
    return(<View style ={{
        flex: 1, alignItems: "center",
        justifyContent: "center"
                        }}>
        <Text>this is Post</Text>
         </View>);
}*/
function Animal (){
    return(<View style ={{
        flex: 1, alignItems: "center",
        justifyContent: "center"
                        }}>
        <Text>this is Animal</Text>
         </View>);
}

const width = Dimensions.get('window').width


export class Post extends React.Component{

  constructor(props){
    super(props)
    this.state={
      fileList:[],
      title:'',
      content:'',
      image:''
    }
  }
  renderItem=({item,index})=>{
    return(
    <View style={styles.inputContent}>
      <Image source={item.url} style={styles.itemImage} />
    </View>)

  };

  onSelectedImage=(image)=>{
    let newImg = this.state.fileList;
    const source ={uri: image.path};
    let item ={
      id: Date.now(),
      url: source,
      content: image.data
    }
    newImg.push(item);
    this.setState({fileList: newImg})
  }

  takePhotoFromCamera(){
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      this.onSelectedImage(image)
      console.log(image);
    });
  }

  choosePhotoFromLibrary(){
    ImagePicker.openPicker({
      compressImageMaxWidth: 300,
      compressImageMaxHeight: 400,
      cropping: true,
      includeBase64: true
    }).then(image => {
      this.onSelectedImage(image)
      console.log(image);
    });
  }
  onClickAddImage=()=>{
    const btn = ['TakePhoto','Choose Photo Library','Cancel'];
    ActionSheet.show(
      {options: btn, cancelButtonIndex:2, title:'Select a photo'},
     buttonIndex=>{
       switch(buttonIndex){
         case 0:
           this.takePhotoFromCamera();
           break;
           case 1:
            this.choosePhotoFromLibrary();
             break;
             default:
               break;
       }

      }
    )

  }

  
  onClickAddPost=()=>{
    const {title} = this.state.title
   Alert.alert(title)
   console.log("title--->",title)
  }
render(){
  let {cont}= styles;
  let {fileList}=this.state;
  
  return(
    
    
    <View style={styles.container}>
    <View style={styles.inputContainer} marginTop={20}>
      <TextInput style={styles.inputs}
        placeholder="Title"
        keyboardType="default"
        underlineColorAndroid='transparent'
        
        onChange={title =>this.setState({title: title})}
        value={this.state.title}
      />
      </View>
      <View style={styles.inputContent}>
      <TextInput style={styles.inputs}
        placeholder="Content"
        multiline={true}
        numberOfLines={4}
        underlineColorAndroid='transparent'
        value={this.state.content}
        onChangeText={(content)=>this.setState({content})}
      />
      </View>
     
      <Root>
      
      <FlatList data={fileList}
                renderItem={this.renderItem}
                onEndReachedThreshold={1}
                keyExtractor={(item,index)=>index.toString()}
                extraData={this.state.fileList}/>
         
         <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={this.onClickAddImage} >
         <Text style={styles.loginText}>Add Image</Text>
         </TouchableHighlight>
   
    </Root>
    <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={this.onClickAddPost} >
         <Text style={styles.loginText}>Add Post</Text>
         </TouchableHighlight>
    </View>
  )
  

  }
}

const styles = StyleSheet.create({
  cont:{
    flex: 1,
      marginTop:50,
      alignItems: 'center',

  },
  container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#DCDCDC',
    },
button: {
  paddingHorizontal: 20,
  paddingVertical: 10,
  marginVertical: 10,
  borderRadius: 5
}
,
inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius:30,
    borderBottomWidth: 1,
    width:250,
    height:45,
    marginBottom:20,
    flexDirection: 'row',
    alignItems:'center'
},
inputContent: {
  borderBottomColor: '#F5FCFF',
  backgroundColor: '#FFFFFF',
  borderRadius:30,
  borderBottomWidth: 1,
  width:250,
  height:150,
  marginBottom:20,
  flexDirection: 'row',
  alignItems:'center'
},
inputs:{
    height:45,
    marginLeft:16,
    borderBottomColor: '#FFFFFF',
    flex:1,
},
content:{
  height:240,
  marginLeft:16,
  borderBottomColor: '#FFFFFF',
  flex:1,
},
inputIcon:{
  width:30,
  height:30,
  marginLeft:15,
  justifyContent: 'center'
},
buttonContainer: {
  height:45,
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  marginBottom:20,
  width:250,
  borderRadius:30,
},
loginButton: {
  backgroundColor: "#45f74b",
},
loginText: {
  color: 'white',
},
itemImage:{
  backgroundColor: '#45f74b',
  height:150,
  width: width-60,
  borderRadius:20,
  borderColor:"#45f74b",
  resizeMode:'contain'
},
avatar: {
  width: 180,
  height: 180,
  borderRadius: 63,
  borderWidth: 4,
  borderColor: "#45f74b",
  
  position:'absolute',
  marginTop:10,
  
},
});