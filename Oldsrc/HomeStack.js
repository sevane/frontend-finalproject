
import React from 'react';
import {View, Text,StyleSheet, FlatList,Image, Dimensions, ActivityIndicator, Button} from "react-native"
import axios from 'axios'
import Icon from 'react-native-vector-icons/Ionicons'
import { createStackNavigator } from "@react-navigation/stack";
import { useNavigation } from '@react-navigation/native';
//import ChatStack from "./ChatStack"

 const Stack = createStackNavigator();

 export const HomeStack =()=>{
    
      return(
          <Stack.Navigator>
              <Stack.Screen name= "Posts" options={{header: ()=>null}} component={Posts}></Stack.Screen>
              <Stack.Screen name= "Comments"  component={Comments}></Stack.Screen>
              <Stack.Screen name= "Chat"  component={Chat}></Stack.Screen>
          </Stack.Navigator>
      );
 }
 function Comments (){
    return(<View style ={{
        flex: 1, alignItems: "center",
        justifyContent: "center"
                        }}>
        <Text>this is Comment</Text>
         </View>);
}
function Chat (){
    return(<View style ={{
        flex: 1, alignItems: "center",
        justifyContent: "center"
                        }}>
        <Text>this is Hello</Text>
         </View>);
}
 
//create component
export default function(props) {
    const navigation = useNavigation();
    return <MyBackButton {...props} navigation={navigation} />;
  }
export  class Posts extends React.Component {

     
 constructor()
 {
     super()
     this.state={
         isLoading: true,
         data:'',
         page:1,
     }
     console.disableYellowBox=true
 }
componentDidMount(){
    this.makeRequest()
}
 makeRequest = ()=>{
     //fetching posts from moch api
     //axios.get('https://5ea428ca270de600164600de.mockapi.io/api/posts?' )
     console.log('before get test 4')
     axios.get('http://10.0.2.2:8000/api/posts')
     .then(res=>{
         //console.log('code response--->',res)
         this.setState({
             isLoading:false,
             data: [...this.state.data,...res.data]
         })
     }).catch(function (error) {
        this.setState({
            isLoading:false
        })
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        }
      })
 }

 handleMore =()=>{
this.setState({
    page: this.state.page +1
    },()=>{
        this.makeRequest()
    })
 }
 renderFooter=()=>{
     return(
         <View>
             <ActivityIndicator animating size="large"/>
         </View>
     )
 }

 render(){
    console.log('render code',this.state.data)
    if(this.state.isLoading){
        return(
            <View style={styles.Load}>
                <ActivityIndicator animating size="large"/>
            </View>
        )
    }
  
   const { navigation } = this.props;
    return(
        <View style={styles.container}>
            <View style={styles.header}>
                    <Text style={styles.headerTitle}>ALIF</Text>
            </View>
           <FlatList data={this.state.data} 
                            keyExtractor={(item)=>item.first_name}
                            onEndReached={this.handleMore}
                            ListFooterComponent={this.renderFooter}
                            onEndReachedThreshold={10}
                            renderItem={({item})=>
                                            <View style={styles.feedItem}>
                                            <Image source ={{uri: item.avatar}} style={styles.avatar}/>
                                            <View style={{flex:1}}>
                                                <View style={{ flexDirection: 'row', justifyContent:"space-between", alignItems:"center"}}>
                                                    <View>
                                                    <Text style ={styles.name}>{item.first_name}</Text>
                                                    </View>
                            
                                                </View>
                                            
                                                <Text style={styles.post}>{item.email}</Text>
                                                <Image source={{uri: item.image}} style={styles.postImage} resizeMode="cover"></Image>
                                                <View style={{flexDirection:'row'}}>
                                                    <Icon.Button name="ios-heart" size={24}  backgroundColor="transparent"  color="#73788B"  ></Icon.Button>
                                                    <Icon.Button name="ios-create" size={24} backgroundColor="transparent"  color="#73788B" onPress={()=>{navigation.navigate("Comments");}} ></Icon.Button>
                                                    <Icon.Button name="ios-chatbubbles" size={24} backgroundColor="transparent"  color="#73788B" onPress={()=>{navigation.navigate("Chat");}} ></Icon.Button>
                                                </View>
                                            </View>
                                        </View>
                            }/>
        </View>
    )
}




}


const styles=StyleSheet.create({
    container:{
        flex: 1, 
       
    },
    header: {
        paddingTop: 14,
        paddingBottom: 16,
        backgroundColor: "#FFF",
        alignItems: "center",
        justifyContent: "center",
        borderBottomWidth: 1,
        borderBottomColor: "#EBECF4",
        shadowColor: "#454D65",
        shadowOffset: { height: 5 },
        shadowRadius: 15,
        shadowOpacity: 0.2,
        zIndex: 10
    },
    headerTitle: {
        fontSize: 20,
        color:'#45f74b',
        fontWeight: "700",
       
    },
    
    name:{
        fontSize:15,
        color:'green',
        marginTop:4
    },
    Load:{
        flex:1,
        padding:20,
        alignItems:'center',
        justifyContent:'center'
    },
    avatar:{
        width:36,
        height:36,
        borderRadius:18,
        marginRight:16

    },
    post:{
        marginTop:16,
        fontSize:14,
        color: "#838899"

    },
    postImage:{
        flex:1,
        width: null,
        height:150,
        borderRadius:5,
        marginVertical:16
    },
    feedItem:{
        backgroundColor:"#FFF",
        borderRadius:5,
        padding:8,
        flexDirection: "row",
        marginVertical:8
    }
    

})
