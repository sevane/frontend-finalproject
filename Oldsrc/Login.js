import React, { Component }  from 'react'
import { createStackNavigator } from "@react-navigation/stack";

import {StyleSheet, Text,View,TextInput,TouchableHighlight,} from 'react-native'
		
    class Login extends Component 
    
    {   
        state = {username:"",
                password:""}


        CheckLogin ()
    {
    //const { username, password}= this.state
    //if(username =="admin" && password == "admin")
    //{  
      let email="sevane@gmail.com";
      let password ="123123";
      let response = fetch('http:localhost:8000/api/login?email=${email}&password=${password}',
      {headers:{ 'Accept': 'application/json',
                 'Content-Type':'application/json'}})
      /*console.log("before fetch")
         fetch('http://185.101.16.197:8000/api/login',
             {
              method:'POST',
             headers:{ 'Accept': 'application/json',
                        'Content-Type':'application/json'},
             body: JSON.stringify({email,password})
              },

             console.log("inside fetch")
             )
             .then(res=>{
                 console.log(res)
                 if(res=='200')
                 { console.log("res 200")
                   this.props.navigation.navigate('Home')}
                 else{
                  console.log("!res 200")
                  Alert.alert('Error', "Incorrect username or password.", [{
                    text: "Ok"}])
                 }
             }).catch(function (error){
               console.log("this is error")
               console.log(error.message)
             })*/
   
        
        //console.warn("Login ok")
   /* }
    else 
    {
        Alert.alert('Error', "Incorrect username or password.", [{
            text: "Ok"
        }]
        )
    }*/
    };
     /*register(){
      return(
      <View style ={{
        flex: 1, alignItems: "center",
        justifyContent: "center"
                        }}>
        <Text>this is Profile</Text>
         </View>);
    }*/

        render(){
		return ( 
            <View style={styles.container}>
            <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
                placeholder="Email"
                keyboardType="email-address"
                underlineColorAndroid='transparent'
                onChangeText={email => this.setState({username: email})}/>
          </View>
          
          <View style={styles.inputContainer}>
               <TextInput style={styles.inputs}
                placeholder="Password"
                secureTextEntry={true}
                underlineColorAndroid='transparent'
                onChangeText={pass => this.setState({password: pass})}/>
          </View>
  
          <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={() => this.CheckLogin()}>
            <Text style={styles.loginText}>Login</Text>
          </TouchableHighlight></View>
         );
        }
        
    }
    const styles = StyleSheet.create({
        container: {
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#DCDCDC',
        },
        inputContainer: {
            borderBottomColor: '#F5FCFF',
            backgroundColor: '#FFFFFF',
            borderRadius:30,
            borderBottomWidth: 1,
            width:250,
            height:45,
            marginBottom:20,
            flexDirection: 'row',
            alignItems:'center'
        },
        inputs:{
            height:45,
            marginLeft:16,
            borderBottomColor: '#FFFFFF',
            flex:1,
        },
        inputIcon:{
          width:30,
          height:30,
          marginLeft:15,
          justifyContent: 'center'
        },
        buttonContainer: {
          height:45,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom:20,
          width:250,
          borderRadius:30,
        },
        loginButton: {
          backgroundColor: "#45f74b",
        },
        loginText: {
          color: 'white',
        }
      });
export default Login