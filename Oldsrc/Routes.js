import React, { useState, useEffect, useContext } from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import {NavigationContainer} from "@react-navigation/native";
import {View, Text, Button, ActivityIndicator, AsyncStorage } from "react-native";
import { AuthContext } from './AuthProvider';
import {Login} from './Login';
import {Tab} from "./Tab"



const Stack = createStackNavigator();
AuthContext = React.createContext();
/*function Login({navigation}) {
    return (
        <View style ={{
            flex: 1, alignItems: "center",
            justifyContent: "center"
        }}>
            <Text>I am Login page</Text>
            <Button title="Go to register" onPress={()=>{navigation.navigate("Register");}}></Button>
        </View>
    )
}
/*
function Login({navigation}){
    return ( 
        <View style={styles.container}>
        <View style={styles.inputContainer}>
        <TextInput style={styles.inputs}
            placeholder="Email"
            keyboardType="email-address"
            underlineColorAndroid='transparent'
            onChangeText={email => this.setState({username: email})}/>
      </View>
      
      <View style={styles.inputContainer}>
           <TextInput style={styles.inputs}
            placeholder="Password"
            secureTextEntry={true}
            underlineColorAndroid='transparent'
            onChangeText={pass => this.setState({password: pass})}/>
      </View>

      <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={() => this.CheckLogin()}>
        <Text style={styles.loginText}>Login</Text>
      </TouchableHighlight>

    <TouchableHighlight style={styles.buttonContainer} onPress={()=>{navigation.navigate("Register");}}>
        <Text>Register</Text>
    </TouchableHighlight>
      </View>
    );
}
function CheckLogin(){

}*/
function Register({navigation}) {
    return (
        <View style ={{
            flex: 1, alignItems: "center",
            justifyContent: "center"
        }}>
            <Text>I am Register page</Text>
            <Button title="Go to Login" onPress={()=>{navigation.navigate("Login")
            ;}}></Button>

        </View>
    )
}
export const Routes =({}) => {

const {user,login,logout} = React.useContext(AuthContext);
   
const [loading, setLoading] = useState(true);
useEffect(()=>{
    //check if user is logged in 
    AsyncStorage.getItem('user')
    .then(userString =>{
       if(userString){
           //decode it
           login();
       }else {
           setLoading(false)
       }
    })
    .catch(err=> {
        console.log(err);
        
    })
})
    if (loading){
        return(
            <View style ={{
                flex: 1, alignItems: "center",
                justifyContent: "center"
            }}>
                <ActivityIndicator size="large" />
            </View>
        )
    }
//export default function Routes () {
    return (

   
        <NavigationContainer>
         
            <Stack.Navigator initialRouteName="Login">
                <Stack.Screen  name ="Login" component={Login}></Stack.Screen>
                <Stack.Screen  name ="Register" options={{header: ()=>null}} component={Register}></Stack.Screen>  
            </Stack.Navigator>
          
        </NavigationContainer>

    
);
    }