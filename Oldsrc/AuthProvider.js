/*import React, { useState } from 'react';
import { AsyncStorage } from 'react-native';
import {Routes} from './Routes'

export const AuthContext = React.createContext();

export const AuthProvider = ({children})=>{
    const [user, setUser]= useState(null);
    const authContext = React.useMemo(() => {
    return {
      signIn: () => {
        setIsLoading(false);
        setUser("asdf");
      },
      signUp: () => {
        setIsLoading(false);
        setUser("asdf");
      },
      signOut: () => {
        setIsLoading(false);
        setUser(null);
      }
    };
  }, []);

  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 1000);
  }, []);

  if (isLoading) {
    return <Splash />;
  }
    return(
        <AuthContext.Provider
        value={{
            user,
            login:()=>{
                const fakeUser={username: "bob"};
                setUser(fakeUser);
                AsyncStorage.setItem("user", JSON.stringify(fakeUser));
            },
            logout: () => {
                AsyncStorage.removeItem("user");
            }
        }}
        >{children}</AuthContext.Provider>
    )
}*/
const AuthStack = createStackNavigator();
const AuthStackScreen = () => (
  <AuthStack.Navigator>
    <AuthStack.Screen
      name="SignIn"
      component={SignIn}
      options={{ title: "Sign In" }}
    />
    <AuthStack.Screen
      name="CreateAccount"
      component={CreateAccount}
      options={{ title: "Create Account" }}
    />
  </AuthStack.Navigator>
);
const RootStack = createStackNavigator();
const RootStackScreen = ({ userToken }) => (
  <RootStack.Navigator headerMode="none">
    {userToken ? (
      <RootStack.Screen
        name="App"
        component={TabsScreen}
        options={{
          animationEnabled: false
        }}
      />
    ) : (
      <RootStack.Screen
        name="Auth"
        component={AuthStackScreen}
        options={{
          animationEnabled: false
        }}
      />
    )}
  </RootStack.Navigator>
);

export default () => {
  const [isLoading, setIsLoading] = React.useState(true);
  const [userToken, setUserToken] = React.useState(null);

  const authContext = React.useMemo(() => {
    return {
      signIn: () => {
        setIsLoading(false);
        setUserToken("asdf");
      },
      signUp: () => {
        setIsLoading(false);
        setUserToken("asdf");
      },
      signOut: () => {
        setIsLoading(false);
        setUserToken(null);
      }
    };
  }, []);

  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 1000);
  }, []);

  if (isLoading) {
    return <Splash />;
  }

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <RootStackScreen userToken={userToken} />
      </NavigationContainer>
    </AuthContext.Provider>
  );
};