import firebase from 'firebase'

export class Chat 
{
    constructor(){
        this.init()
        
    }
    init =()=>
    {
        if(!firebase.apps.length)
        {

        firebase.initializeApp({
        apiKey: "AIzaSyABrjfdwkTiA5Pv32fLt580fu-mr4V-N7c",
        authDomain: "myreact-4d92e.firebaseapp.com",
        databaseURL: "https://myreact-4d92e.firebaseio.com",
        projectId: "myreact-4d92e",
        storageBucket: "myreact-4d92e.appspot.com",
        messagingSenderId: "29226912365",
        appId: "1:29226912365:web:e0b2e1732c4b63dff94d64",
        measurementId: "G-W837YEXJQV"})
        };
    }
    send = messages =>
    {
        messages.forEach(item => {
            const message ={
                text: item.text,
                timestamp: firebase.database.ServerValue.TIMESTAMP,
                user: item.user 
            };
            this.db.push(messages)
        });

    };

    parse = message =>{
        const {user,text,timestamp}=message.val();
        const {key: _id}= message;
        const createdAt = new Date(timestamp);

        return{
            _id, createdAt,text,user
        }
    }

    get = callback =>{
        this.db.on("child_added", snapshot=> callback(this.parse(snapshot)))
    }

    off(){
        this.db.off();
    }
    get uid(){
        return (firebase.auth().currentUser || {}).uid
    }

    get db()
    {
        return firebase.database().ref("messages");

    }
}

export default new Chat();